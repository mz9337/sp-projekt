# BRALNIK RSS VIROV

SPLOŠNI OPIS

Ob vstopu v program se uporabniku prikaže seznam RSS virov, razvrščen po oceni virov padajoče.  
Ocena vira je pridobljena kot povprečje ocen njegovih novic. Uporabnik ima možnost vpogleda zadnjih  
novic teh virov. Po virih lahko tudi išče s pomočjo iskalnika nad sezanmom RSS virov.  
  
S klikom na "Prijava" v zgorjnem desnem kotu zaslona, se lahko uporabnik prijavi v program. Če računa še nima  
ga lahko ustvari z klikom na povezavo: "registriraj". Takoj po prijavi se mu prikaže seznam RSS virov, ki jim sledi.  
Na seznamu lahko uporabnik vidi za vsak vir njegovo oceno in število še neprepbranih novic. S klikom na modri gumb ob  
viru lahko uporabnik pregleda njegove zadnje novice, s klikom na rdečega pa odstrani vir iz seznama virov ki jim sledi.

Uporabnik lahko sledi novemu viru na dva načina:  

* s klikom na gumb "Dodaj RSS vir", kjer lahko vnese URL željenega vira 
* s klikom na gumb "Priljubljeni viri" v glavi strani, ki ga pripelje na seznam najbolj priljubljenih virov. Tam lahko  
   uporabnik pregleduje vse vire ki so jih durgi uporabniki vnesli v bazo preko gumba "Dodaj RSS vir" in se na njih  
   naroči s klikom na rumeni plus. Viri na katere je uporabnik že naročen, bodo imeli namesto rumenega plusa zeleno kljukico. 
   
S klikom na gumb "Moj profil" v glavi lahko uporabnik ureja svoje osebne podatke.

**Pri testiranju v različnih brskalnikih smo ugotovili, da Mozilla Firefox pri lokalnem izvajanju ne podpira znakov, generiranih s Font Awesome.**

Povezave in opis posameznih zaslonskih mask:

* [index.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/index.html)
    - Predstavlja stran, na katero uporabnik pride, ko še ni prijavljen - vidi vire, ki so razvrščeni po oceni in lahko vidi novice iz teh virov.
    - Uporabnik se lahko registrira ali prijavi.
    
* [login.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/login.html)
* [registration.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/registration.html)
    - S klikom na "Prijava" v zgornjem desnem kotu zaslona, se lahko uporabnik prijavi v program. Če računa še nima  
ga lahko ustvari z klikom na povezavo: "registriraj". Takoj po prijavi se mu prikaže seznam RSS virov, ki jim sledi. 

* [my_profile.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/my_profile.html)
    - Prikaz "mojih RSS virov" - prikazani so viri, na katere je uporabnik naročen.
    - Uporabnik ima možnost iskanja po RSS virih, lahko si ogleda novice s posameznega vira, lahko pa tudi zbriše naročnino na posamezni vir.
    - V zelenem okvirju je prikazano število novic s posameznega vira, ki so se pojavile od zadnjega uporabnikovega vpogleda v ta vir.

* [new_rss_feed.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/new_rss_feed.html)
    - Ta zaslonska maska omogoča dodajanje novega RSS vira. Za dodajanje je potrebno vnesti URL do vira RSS ter ime, opis in kategorijo.
    - Nov RSS vir se doda na seznam vseh virov, kjer ga lahko ostali uporabniki dodajo med svoje vire.

* [rss_feed.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/rss_feed.html)
    - Ta zaslonska maska predstavlja novice, pridobljene s posameznega RSS vira
    - Uporabnik lahko oceni posamezno novico ter jo deli na Facebook in Twitter profil.

* [search_popular_feeds.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/search_popular_feeds.html)
    - Ta zaslonska maska predstavlja seznam priljubljenih RSS virov. Seznam sicer vsebuje vse vire, ki so jih vnesli uporabniki,
in so razporejeni glede na skupno oceno vseh uporabnikov.

* [user_profile.html](https://bitbucket.org/mz9337/sp-projekt/src/master/docs/user_profile.html)
    - S klikom na gumb "Moj profil" v glavi lahko uporabnik ureja svoje osebne podatke.
_____________________________________________________________________________________________________________________________________________________________

Spletna stran vsebuje tudi integracijo aplikacije z zunanjim virom --> API **TheySaidSo** za Daily Inspirational Quotes, ki so prikazani na dnu zavihkov "Moji RSS viri" in "Priljubljeni RSS viri".

- Spletna stran deluje na naslednjih napravah:
    - telefon (testirano tako na iOS kot Android napravah)
    - računalnik
    - tablica

**Seznam dovoljenih vnosov za vsa uporabniška vnosna polja:**

* [registracija.pug](https://bitbucket.org/mz9337/sp-projekt/src/master/NewsCatcher/app_server/views/registracija.pug)
	- vnosna polja: 
		- "Vnesi ime": vnosno polje lahko vsebuje velike in male črke, piko in presledek. Z minimalno dolžino 2 in maksimalno 25.
			opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
		- "Vnesi priimek":  vnosno polje lahko vsebuje velike in male črke, piko in presledek. Z minimalno dolžino 2 in maksimalno 25.
			opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
		- "Vnesi email" vnosno polje lahko vsebuje: pred @ tako velike kot male črke, za @ tako velike kot male črke, piko in ime domene, ki je lahko velikih ali malih črk.
			opis v kodi: pattern='^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'
		- "Vnesi uporabniško ime":  vnosno polje lahko vsebuje velike in male črke, piko in presledek. Z minimalno dolžino 2 in maksimalno 25.
			opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
		- "Vnesi geslo":  vnosno polje Geslo lahko vsebuje velike male črke, piko in presledek!  Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi:  pattern='^(?![ .]+$)[a-zA-Z .]*$'
* [viri-dodaj-nov.pug](https://bitbucket.org/mz9337/sp-projekt/src/master/NewsCatcher/app_server/views/viri-dodaj-nov.pug)
    - vnosna polja: 
        - "Vnesite URL RSS kanala": vnosno polje lahko dovoljuje večino standardiziranih kombinaciji URL zapisov
            opis v kodi: pattern='((http|https):\/\/)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)'
        - "Vnesite ime RSS kanala": vnosno polje lahko vsebuje velike, male črke ter številke. Omejeno je z minimalno dolžino 5 in maksimalno dolžino 25
            opis v kodi: pattern='[A-Za-z0-9]+'
        - "Vnesite opis RSS kanala": vnosno polje lahko odamo prozano ter z poljubnim nizom do dolžine 50-tih znakov.
* [uporabnik-profil.pug](https://bitbucket.org/mz9337/sp-projekt/src/master/NewsCatcher/app_server/views/uporabnik-profil.pug)
    - vnosna polja: 
        - "Vnesi ime": vnosno polje lahko vsebuje velike in male črke, piko in presledek. Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
        - "Vnesi priimek": vnosno polje lahko vsebuje velike in male črke, piko in presledek.  Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
        - "Vnesi uporabniško ime":vnosno polje lahko vsebuje velike in male črke, piko in presledek.  Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
        - "Vnesi geslo":  vnosno polje Geslo lahko vsebuje velike male črke, piko in presledek!  Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
* [prijava.pug](https://bitbucket.org/mz9337/sp-projekt/src/master/NewsCatcher/app_server/views/prijava.pug)
    - vnosna polja: 
        - "Vnesi eMail": vnosno polje lahko vsebuje pravilen vnost emaila.
            opis v kodi: pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$'
        - "Vnesi geslo": vnosno polje Geslo lahko vsebuje velike male črke, piko in presledek!  Z minimalno dolžino 2 in maksimalno 25.
            opis v kodi: pattern='^(?![ .]+$)[a-zA-Z .]*$'
_____________________________________________________________________________________________________________________________________________________________
Povezava na delujočo spletno aplikacijo na Heroku: https://bralnik-rss-virov.herokuapp.com/

- Navodila: 
    - Odpremo c9.io in kreiramo novo delovno okolje
    - Kloniramo projekt iz https://bitbucket.org/mz9337/sp-projekt/src/master/ (clone → HTTPS → skopiramo povezavo in vnesemo v terminal)
    - V isti terminal vnesemo ukaz git clone, h kateremu prilepimo skopiran ukaz iz prejšnje točke
        - Primer: git clone https://vg0691@bitbucket.org/mz9337/sp-projekt.git 
    - Premaknemo se v mapo NewsCatcher
    - Nameščanje odvisnosti
        - $ npm install
    - Odpremo nov terminal in izvedemo naslednje ukaze:
        - $ sudo apt-get remove mongodb-org mongodb-org-server
        - $ sudo apt-get autoremove
        - $ sudo rm -rf /usr/bin/mongo*
        - $ sudo rm /etc/apt/sources.list.d/mongodb*.list
        - $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
        - $ echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
        - $ sudo apt-get update
        - $ sudo apt-get install mongodb-org mongodb-org-server
        - $ sudo touch /etc/init.d/mongod
        - $ sudo apt-get install mongodb-org-server    
        - $ cd ~/workspace
        - $ mkdir mongodb
        - $ cd mongodb
        - $ mkdir data
        - $ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal "$@"' > mongod
        - $ chmod a+x mongod
        - $ cd ~/workspace/mongodb
        - $ ./mongod
    - Se vrnemo v prejšnji terminal in:
        - Poženemo strežnik z $ npm start
    - Odpremo preview -> preview running application
        - V lokalni browser naše aplikacije vnesemo: npr. https://sp-projekt-vg0691.c9users.io/db in kliknemo na “Testni podatki”
        - Odpremo še enkrat naslov: https://sp-projekt-vg0691.c9users.io/
    - ...testiranje aplikacije
_____________________________________________________________________________________________________________________________________________________________
* Vrste uporabnikov in njihov pogled:
    - Anonimni/neprijavljen uporabnik: 
        - vidi uvodno stran 'Priljubljeni viri', kjer lahko brska po obstoječem seznamu virov, ima pa možnost tudi iskanja virov; ko izbere določen vir, se mu odprejo novice za izbran vir
        - v navigacijski vrstici vidi možnosti 'NewsCatcher' (ta preusmeri na priljubljene vire), 'Prijava' in 'Registracija'
    - Prijavljen uporabnik:
        - vidi uvodno stran 'Priljubljeni viri', kjer lahko brska po obstoječem seznamu virov, ima pa možnost tudi iskanja virov; ko izbere določen vir, se mu odprejo novice za izbran vir
        - po prijavi vidi drugačno stran 'Priljubljeni viri', saj se lahko tudi naroča na vse zlistane vire
        - v navigacijski vrstici vidi možnosti 'NewsCatcher' (ta preusmeri na uporabnikove vire), 'Moji viri', 'Priljubljeni viri', 'Moj profil' in 'Odjava'
        - po kliku na 'Moji viri' ima na seznamu samo vire, na katere se je naročil; hkrati pa lahko na tej strani dodaja tudi nove vire, na katere pa je potem tudi avtomatsko naročen
        - odnaročanje od virov je možno tako na strani 'Moji viri' kot 'Priljubljeni viri'
        - ima tudi možnost ogleda svojega profila in urejanja obstoječih določenih podatkov (razen emaila) in posodobitev gesla
    - Administrator:
        - vidi samo stran z vsemi možnimi viri, katere lahko pregleduje, lahko pa jih tudi izbriše
        - v navigacijski vrstici vidi možnosti 'NewsCatcher' (ta preusmeri samo na seznam virov) in 'Odjava'
___________________________________________________________        
        
* Časi nalaganja strani v brskalnikih Chrome in Mozilla:
    - Chrome: 
	    - /: 0.21 s
	    - /registracija: 0.25 s
	    - /prijava: 0.17 s 
	    - /viri/brskaj: 0.26 s
	    - /uporabnik: 0.28 s
	    - /db: 0.23 s
	    - /admin: 0.27 s
	    - /viri/5c364f7a26fd8c732ccaa5d4: 0.18 s

	    **profil uporabnika se naloži najpočasneje**, ker se gradniki input v povprečju 
        počasneje nalagajo in pred izgradnjo strani zahtevamo vračilo podatkov o uporabniku preko REST-apija

    - Mozilla:
	    - /: 0.14 s
	    - /registracija: 0.13 s
	    - /prijava: 0.11 s 
	    - /viri/brskaj: 0.10 s
	    - /uporabnik: 0.19 s
	    - /db: 0.18 s
	    - /admin: 0.12 s
	    - /viri/5c3afc902af35e1c9c30c9f7: 0.10 s

	    **profil uporabnika se naloži najpočasneje**, ker se gradniki input v povprečju 
        počasnejše nalagajo in pred izgradnjo strani zahtevamo vračilo podatkov o uporabniku preko REST-apija
    
____________________________________________________________

* OWASP ZAP poročilo:
    - rešeni vsi alerti, razen cross-domain javascript source file inclusion, saj imamo vključen 'third party' zunanji API in se alert nanaša nanj
____________________________________________________________

* Apache JMeter
    - Podatki o računalniku:
	    - Procesor: Intel(R) Core(TM) i7-4720HQ CPU @ 2.60GHz 2.59GHz, 2594 MHz, 4 Cores
	    - RAM: 12,0 GB
	    - disk: HGST HTS541010A9E680 ATA Device, 512 Bytes/Sector, 3 partitions, 931,51 GB size
    - podatki o zmogljivosti (koliko hkratnih uporabnikov je sposoben predelati naš strežnik):
        - aplikacijo smo testirali na enem računalniku (podatki zgoraj)
        - za zahtevek se je uporabila GET zahteva na uvodno stran naše aplikacije ('/')
        - 'Thread properties' - nastavitve niti so se spreminjale, sepravi število niti/uporabnikov se je postopoma večalo, da smo poskusili ugotoviti, kje aplikcaija počepne
        - nastavitev za urnik (scheduler conf.) je bila nastvljena na 300 s (5 min)
        - 'ramp-up period' (povečanje aktivnosti): nastavljen skozi celoten test na 120 s (2 min)
        - število niti/uporabnikov smo začeli pri 50, nadaljevali s 100, 500, 1000, 5000, 10 000, 30 000; pri slednji je aplikacija obstala in v konzoli se je izpisalo, da je največje št. uporabnikov lahko okoli 22 000 - 23 000.
        - za izbrane vrednosti parametrov smo se odločili na podlagi tega, kakšno zahtevo smo izvajali (GET na '/') in smo poskusili, koliko uporabnikov (prijavljenih ali ne) lahko naenkrat dostopa do naše aplikacije.
