var express = require('express');
var router = express.Router();
var viriController = require('../controllers/viri');
var noviceController = require('../controllers/novice');
var oceneController = require('../controllers/ocene');
var uporabnikiController = require('../controllers/uporabniki');
var bazaController = require('../controllers/baza');
var avtentikacijaController = require('../controllers/avtentikacija');

/* Baza */
router.post('/db/ustvari', bazaController.ustvari);
router.post('/db/izbrisi', bazaController.izbrisi);
router.post('/db/rss', bazaController.rss);

/* Viri */
router.get('/viri', viriController.viriSeznam);
router.post('/viri', viriController.viriKreiraj);
router.get('/viri/:idVira', viriController.viriPreberi);
router.put('/viri/:idVira', viriController.viriPosodobi);
router.delete('/viri/:idVira', viriController.viriIzbrisi);

/* Ocene */
router.get('/viri/:idVira/ocene', oceneController.oceneSeznam);
router.post('/viri/:idVira/ocene', oceneController.oceneKreiraj);
router.get('/viri/:idVira/ocene/:idUporabnika', oceneController.ocenePreberi);
router.put('/viri/:idVira/ocene/:idUporabnika', oceneController.ocenePosodobi);
router.delete('/viri/:idVira/ocene/:idUporabnika', oceneController.oceneIzbrisi);

/* Novice */
router.get('/viri/:idVira/novice', noviceController.noviceSeznam);
router.post('/viri/:idVira/novice', noviceController.noviceKreiraj);
router.get('/viri/:idVira/novice/:idNovice', noviceController.novicePreberi);
router.put('/viri/:idVira/novice/:idNovice', noviceController.novicePosodobi);
router.delete('/viri/:idVira/novice/:idNovice', noviceController.noviceIzbrisi);

/* Uporabniki */
router.get('/uporabniki', uporabnikiController.uporabnikiSeznam);	 
router.post('/uporabniki', uporabnikiController.uporabnikiKreiraj);	 
router.get('/uporabniki/:idUporabnika', uporabnikiController.uporabnikiPreberi);	 
router.put('/uporabniki/:idUporabnika', uporabnikiController.uporabnikiPosodobi);	 
router.delete('/uporabniki/:idUporabnika', uporabnikiController.uporabnikiIzbrisi);	 
router.get('/uporabniki/:idUporabnika/viri', uporabnikiController.uporabnikiViri);
router.post('/uporabniki/:idUporabnika/viri', uporabnikiController.uporabnikiDodajVir);
router.delete('/uporabniki/:idUporabnika/viri/:idVira', uporabnikiController.uporabnikiIzbrisiVir);

/* Avtentikacija */
router.post('/registracija', avtentikacijaController.registracija);
router.post('/prijava', avtentikacijaController.prijava);

module.exports = router;