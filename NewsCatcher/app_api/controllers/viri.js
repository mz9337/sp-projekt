var mongoose = require('mongoose');
var Vir = mongoose.model('Vir');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.viriSeznam = function(zahteva, odgovor) {
	var isci = (typeof zahteva.query.isci !== "undefined") ? zahteva.query.isci : '';

	var aggregate = Vir
		.aggregate([
			{
				$match: { 
					naziv: { $regex: new RegExp(".*" + isci + ".*", "i") },
				}
			},
		    { 
		    	"$unwind": {
		    		path: "$ocene", 
		    		preserveNullAndEmptyArrays: true
		    	} 
		    },
		    { 
		    	$group : { 
		    		_id: "$_id", 
		    		naziv: { $first: "$naziv" },
		    		opis: { $first: "$opis" },
		    		ocena : {  $avg : "$ocene.ocena" },
		    		url: { $first: "$url" },
		    		ikona: { $first: "$ikona" },
		    		novice: { $first: "$novice" }
		    	} 
		    },
		    { 
		    	$sort : { 
		    		ocena : -1, 
		    		naziv: 1 
		    	} 
		    }
		]);

	var stran = (typeof zahteva.query.stran !== "undefined") ? zahteva.query.stran : 1;
	var naStran = (typeof zahteva.query.naStran !== "undefined") ? zahteva.query.naStran : 10;

	var options = { page : stran, limit : naStran};

	Vir.aggregatePaginate(aggregate, options, function(napaka, viri, pageCount, count) {
		// console.log(pageCount, count);
		if(napaka) {
			vrniJsonOdgovor(odgovor, 500, napaka);
		} else { 
			var result = {
				steviloStrani: pageCount,
				steviloVirov: count,
				viri: viri 
			}
			vrniJsonOdgovor(odgovor, 200, result);
		}
	})
};

module.exports.viriKreiraj = function(zahteva, odgovor) {
	Vir.create({
			naziv: zahteva.body.naziv,
			url: zahteva.body.url,
			opis: zahteva.body.opis,
			ikona: zahteva.body.ikona,
			novice: []
		}, function(napaka, vir) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, vir);
		}
	});
};

module.exports.viriPreberi = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idVira) {
		Vir.findById(zahteva.params.idVira)
		    .exec(function(napaka, rezultat) {
		   	    if (!rezultat) {
	           	   	vrniJsonOdgovor(odgovor, 404, {
	              		"sporočilo": "Ne najdem vira s podanim enoličnim identifikatorjem idVira."
	           	   	});
	          	   	return;
	            } else if (napaka) {
	           	   	vrniJsonOdgovor(odgovor, 500, napaka);
	          	   	return;
	            }

	           	var ocena = 0;

           	    if (typeof rezultat.ocene !== "undefined" && rezultat.ocene.length > 0) {
           			rezultat.ocene.forEach(function(ocenaTmp) {
           				ocena += ocenaTmp.ocena;
           			});

           			ocena /= rezultat.ocene.length;
           			ocena = Math.round(ocena * 100) / 100;
           		}

           		if (typeof rezultat.novice !== "undefined") {
	           		rezultat.novice.sort(function(a,b){
						return new Date(b.datumObjave) - new Date(a.datumObjave);
					});
					rezultat.novice = rezultat.novice.slice(0, 10);
           		}

           		var vir = {
					_id: rezultat._id,
					naziv: rezultat.naziv,
					opis: rezultat.opis,
					ocena: ocena,
					url: rezultat.url,
					ikona: rezultat.ikona,
					novice: ((typeof rezultat.novice !== "undefined") ? rezultat.novice : []),
					ocene: rezultat.ocene
				};

		   	    vrniJsonOdgovor(odgovor, 200, vir);
		   });
	} else {
	    vrniJsonOdgovor(odgovor, 400, { 
	      "sporočilo": "Manjka enolični identifikator idVira!"
	    });
	}
};

module.exports.viriPosodobi = function(zahteva, odgovor) {

	if (!zahteva.params.idVira) {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem vira, idVira je obvezen parameter"
		});
		return;
	}
	Vir
		.findById(zahteva.params.idVira)
		.exec(
			function(napaka, vir) {
				if (!vir) {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ne najdem vira."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				vir.naziv = (typeof zahteva.body.naziv !== "undefined") ? zahteva.body.naziv : vir.naziv;
				vir.url = (typeof zahteva.body.url !== "undefined") ? zahteva.body.url : vir.url;
				vir.opis = (typeof zahteva.body.opis !== "undefined") ? zahteva.body.opis : vir.opis;
				vir.ikona = (typeof zahteva.body.ikona !== "undefined") ? zahteva.body.ikona : vir.ikona;

				vir.save(function(napaka, vir) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, vir);
					}
				});
			}
		);
};

module.exports.viriIzbrisi = function(zahteva, odgovor) {

	var idVira = zahteva.params.idVira;
	if (idVira) {
        Uporabnik
			.updateMany({}, {$pull: {"viri": idVira}})
			.exec(
                function(napaka, uporabniki) {
                	if (napaka) {
                        vrniJsonOdgovor(odgovor, 500, napaka);
                        return;
                    }

                    Vir
                    	.findByIdAndRemove(idVira)
                        .exec(
                    	    function(napaka, vir) {
                    	    	if (napaka) {
                    	       		vrniJsonOdgovor(odgovor, 404, napaka);
                    	        	return;
                    	      	}
                    	    	vrniJsonOdgovor(odgovor, 200, { "status": "uspešno" });
                    	    }
                        );
                }
			)
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem vira, idVira je obvezen parameter."
		});
	}
};