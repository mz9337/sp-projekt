var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');
var Vir = mongoose.model('Vir');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.uporabnikiSeznam = function(zahteva, odgovor) {
	Uporabnik
		.find()
	    .exec(function(napaka, uporabniki) {
	   		if (napaka) {
           		vrniJsonOdgovor(odgovor, 500, napaka);
          	   	return;
           	}

           	vrniJsonOdgovor(odgovor, 200, uporabniki);
	    });
};

module.exports.uporabnikiKreiraj = function(zahteva, odgovor) {
	Uporabnik.create({
			ime: zahteva.body.ime,
			priimek: zahteva.body.priimek,
			uporabniskoIme: zahteva.body.uporabniskoIme,
			email: zahteva.body.email,
			geslo: zahteva.body.geslo,
			viri: [],
			role: (typeof zahteva.body.role !== "undefined") ? zahteva.body.role : "user"
		}, function(napaka, uporabnik) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, uporabnik);
		}
	});
};

module.exports.uporabnikiPreberi = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idUporabnika) {
		Uporabnik
			.findById(zahteva.params.idUporabnika)
		    .exec(function(napaka, uporabnik) {
		   	    if (!uporabnik) {
	           	   	vrniJsonOdgovor(odgovor, 404, {
	              		"sporočilo": "Ne najdem uporabnika s podanim enoličnim identifikatorjem idUporabnika."
	           	   	});
	          	   	return;
	            } else if (napaka) {
	           	   	vrniJsonOdgovor(odgovor, 500, napaka);
	          	   	return;
	            }

		   	    vrniJsonOdgovor(odgovor, 200, uporabnik);
		   });
	} else {
	    vrniJsonOdgovor(odgovor, 400, { 
	      "sporočilo": "Manjka enolični identifikator idUporabnika!"
	    });
	}
};

module.exports.uporabnikiPosodobi = function(zahteva, odgovor) {

	if (!zahteva.params.idUporabnika) {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem uporabnika, idUporabnika je obvezen parameter"
		});
		return;
	}
	Uporabnik
		.findById(zahteva.params.idUporabnika)
		.exec(
			function(napaka, uporabnik) {
				if (!uporabnik) {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ne najdem uporabnika."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				uporabnik.ime = (typeof zahteva.body.ime !== "undefined") ? zahteva.body.ime : uporabnik.ime;
				uporabnik.priimek = (typeof zahteva.body.priimek !== "undefined") ? zahteva.body.priimek : uporabnik.priimek;
				uporabnik.uporabniskoIme = (typeof zahteva.body.uporabniskoIme !== "undefined") ? zahteva.body.uporabniskoIme : uporabnik.uporabniskoIme;
				uporabnik.email = (typeof zahteva.body.email !== "undefined") ? zahteva.body.email : uporabnik.email;
				uporabnik.role = (typeof zahteva.body.role !== "undefined") ? zahteva.body.role : uporabnik.role;
				uporabnik.nastaviGeslo((typeof zahteva.body.geslo !== "undefined") ? zahteva.body.geslo : uporabnik.geslo);

				uporabnik.save(function(napaka, uporabnik) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, uporabnik);
					}
				});
			}
		);
};

module.exports.uporabnikiIzbrisi = function(zahteva, odgovor) {

	var idUporabnika = zahteva.params.idUporabnika;
	if (idUporabnika) {
		Uporabnik
			.findByIdAndRemove(idUporabnika)
		    .exec(
			    function(napaka, uporabnik) {
			    	if (napaka) {
			       		vrniJsonOdgovor(odgovor, 404, napaka);
			        	return;
			      	}
			    	vrniJsonOdgovor(odgovor, 200, { "status": "uspešno" });
			    }
		    );
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem uporabnika, idUporabnika je obvezen parameter."
		});
	}
};

module.exports.uporabnikiViri = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idUporabnika) {
		var stran = (typeof zahteva.query.stran !== "undefined") ? zahteva.query.stran : 1;
		var naStran = (typeof zahteva.query.naStran !== "undefined") ? zahteva.query.naStran : 10;
		var isci = (typeof zahteva.query.isci !== "undefined") ? zahteva.query.isci : '';

		var skip = (stran-1)*naStran;
		var limit = naStran;

		Uporabnik
			.findById(zahteva.params.idUporabnika)
			.exec(function(napaka, uporabnik) {

				if (napaka) {
	           		vrniJsonOdgovor(odgovor, 500, napaka);
	          	   	return;
	           	}

	           	if (!uporabnik) {
	           		vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Ne najdem uporabnika s tem id-jem." });
	           		return;
	           	} 

	           	var steviloVirov = uporabnik.viri.length;

				Uporabnik
					.findById(zahteva.params.idUporabnika)
					// .populate('viri')
					.populate(
						{ 
							path: 'viri',
							match: { 
								naziv: { $regex: new RegExp(".*" + isci + ".*", "i") },
							},
							options: { 
								sort: { 'naziv': 1 },
								skip: skip,
		            			limit : limit
							} 
						})
					.select('viri')
					.exec(function(napaka, uporabnik) {
						if (napaka) {
			           		vrniJsonOdgovor(odgovor, 500, napaka);
			          	   	return;
			           	}

			           	if (!uporabnik) {
			           		vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Ne najdem uporabnika s tem id-jem." });
			           		return;
			           	} 

			           	var viri = [];
			           	uporabnik.viri.forEach(function(dokument) {
			           		var ocena = 0;

			           		if (typeof dokument.ocene !== "undefined" && dokument.ocene.length > 0) {
			           			dokument.ocene.forEach(function(ocenaTmp) {
			           				ocena += ocenaTmp.ocena;
			           			});

			           			ocena /= dokument.ocene.length;
			           			ocena = Math.round(ocena * 100) / 100;
			           		}

							viri.push({
								_id: dokument._id,
								naziv: dokument.naziv,
								opis: dokument.opis,
								ocena: ocena,
								url: dokument.url,
								ikona: dokument.ikona,
								novice: ((typeof dokument.novice !== "undefined") ? dokument.novice : 0),
								ocene: dokument.ocene
							});
						});

						var result = {
							steviloStrani: Math.ceil(steviloVirov/naStran),
							steviloVirov: steviloVirov,
							viri: viri 
						}
			           	
			           	vrniJsonOdgovor(odgovor, 200, result);
					});
			});

	} else {
		vrniJsonOdgovor(odgovor, 400, { 
	    	"sporočilo": "Manjka enolični identifikator idUporabnika!"
	    });
	}
};

module.exports.uporabnikiDodajVir = function(zahteva, odgovor) {
	var idUporabnika = zahteva.params.idUporabnika;
	var idVira = zahteva.body.idVira;
	
	if (idUporabnika) {
		Uporabnik
			.findById(idUporabnika)
			.exec(
				function(napaka, uporabnik) {
					if (napaka) {
				    	vrniJsonOdgovor(odgovor, 400, napaka);
				    	return;
				  	} 

				  	Vir
						.findById(idVira)
						.exec(function(napaka, vir) {
							if (!vir) {
								vrniJsonOdgovor(odgovor, 404, {
								  "sporočilo": "Ne najdem vira s tem id-jem."
								});
							} else {
								uporabnik.viri.push(idVira);
								uporabnik.save(function(napaka, uporabnik) {
									if (napaka) {
										vrniJsonOdgovor(odgovor, 400, napaka);
									} else {
										vrniJsonOdgovor(odgovor, 201, vir);
									}
								});
							}
						});
				  	
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, { 
	    	"sporočilo": "Manjka enolični identifikator idUporabnika!"
	    });
	}
};

module.exports.uporabnikiIzbrisiVir = function(zahteva, odgovor) {
	if (!zahteva.params.idUporabnika || !zahteva.params.idVira) {
		vrniJsonOdgovor(odgovor, 400, {
		  "sporočilo": "Ne najdem uporabnika oz. vira, idUporabnika in idVira sta obvezna parametra."
		});

		return;
	}

	Uporabnik
		.findById(zahteva.params.idUporabnika)
		.exec(
			function(napaka, uporabnik) {
				if (!uporabnik) {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "uporabnika ne najdem."
				  	});
				  	return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				if (uporabnik.viri && uporabnik.viri.length > 0) {
					var virIndex = uporabnik.viri.indexOf(zahteva.params.idVira); 
					if (virIndex == -1) {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "vira ne najdem."
						});
					} else {
						uporabnik.viri.splice(virIndex, 1);
						uporabnik.save(function(napaka) {
							if (napaka) {
								vrniJsonOdgovor(odgovor, 500, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, { "status": "uspešno" });
							}
						});
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "Ni vira za brisanje."
				  	});
				}
			}
		);
};