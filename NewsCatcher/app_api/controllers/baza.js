var mongoose = require('mongoose');
var Vir = mongoose.model('Vir');
var Uporabnik = mongoose.model('Uporabnik');
var db = mongoose.connection;
var rssController = require('../../app_server/controllers/rss');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.ustvari = function(zahteva, odgovor) {
	Uporabnik.create(uporabnikiData, function(napaka, uporabnik) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			Vir.create(viriData, function(napaka, uporabnik) {
				if (napaka) {
					vrniJsonOdgovor(odgovor, 400, napaka);
				} else {
          			rssController.parseRss();
					vrniJsonOdgovor(odgovor, 200, { 
				    	"sporočilo": "Uspešno ustvaril podatke!"
				    });
				}
			});
		}
	});
};

module.exports.izbrisi = function(zahteva, odgovor) {
	Vir.deleteMany({}, function(err) { 
		Uporabnik.deleteMany({}, function(err) { 
	    	vrniJsonOdgovor(odgovor, 200, { 
		    	"sporočilo": "Podatki izbrisani!"
		    });
		});
	});
};

module.exports.rss = function(zahteva, odgovor) {
	rssController.parseRss();
	vrniJsonOdgovor(odgovor, 200, { 
    	"sporočilo": "Pridobivam novice iz virov"
    });
};


// var uporabnikiData = [
// 	{
// 		ime: "Miha",                
// 		priimek: "Zadravec",
// 		uporabniskoIme: "miha",
// 		email: "miha@gmail.com",
// 		geslo: "38884b0272d5b88842766f05f96f03bd35553ce394664fa5264512d2abdcfe91",
// 	},
// 	{
// 		ime: "Vanesa",                
// 		priimek: "Gradišar",
// 		uporabniskoIme: "vanesa",
// 		email: "vanesa@gmail.com",
// 		geslo: "vanesa",
// 	},
// 	{
// 		ime: "Matic",                
// 		priimek: "Anžič",
// 		uporabniskoIme: "matic",
// 		email: "matic@gmail.com",
// 		geslo: "0db6736d471c69e86b9cda3ae1862b24314bbd63661002c4e8b0e14cb7271c73",
// 	},
// 	{
// 		ime: "Alan",                
// 		priimek: "Hadzic",
// 		uporabniskoIme: "alan",
// 		email: "alan@gmail.com",
// 		geslo: "72108d63477606836a8b9e1c71455565c983f763b88fd80f0c1dabb1d982bf7c",
// 	}
// ];

var uporabnikiData = [
	{
        ime: "Miha",
        priimek: "Zadravec",
        uporabniskoIme: "miha",
        email: "miha@gmail.com",
        nakljucnaVrednost: "798dffb665cd4d824f0a7e6bcaf5f286",
        geslo: "22469cc4ba5fa1a25236c30350997da219638e9b019b1dd95f27b83430b6b9bf951599e14120aa6774d415fc58c5d6016ab459773a3c0f97c353ccba58295ef2",
    	role: "user",
    },
    {
        ime: "Vanesa",
        priimek: "Gradišar",
        uporabniskoIme: "vanesa",
        email: "vanesa@gmail.com",
        nakljucnaVrednost: "8550ad02da7729306e6fd60e0c276080",
        geslo: "39ccbdc52b02aae8a466b29fe0de4a2cb866b80658bc6fcebed2b75cc981394e28cdb00a63c6e391c6e9de13b9e80de101263d27e2fcacbb2b10c84ebd30f2bc",
    	role: "user",
    },
    {
        ime: "Matic",
        priimek: "Anžič",
        uporabniskoIme: "matic",
        email: "matic@gmail.com",
        nakljucnaVrednost: "660be2a6d2c885ce69e31f599ea441fa",
        geslo: "4d006751e45c09121f325dc34f3fbe2f06bb127ab652f26e626ebc520a5a4920cbabb00f2fe91c4fcf3e30c6046ba50c3e098ce7d54d9f49c22f7c7d3569e8ce",
    	role: "user",
    },
    {
        ime: "Alan",
        priimek: "Hadzic",
        uporabniskoIme: "alan",
        email: "alan@gmail.com",
        nakljucnaVrednost: "7684e4d36da2dd67201bd7015ef8e02e",
        geslo: "83e6d6d107e8da6a64edcccbd6685c927c8b091bdf3ae51f5b69cbb9e99444707aa4d65d0f0902480e5c2bc97cba3a5e39cca4ef7cf30c0579f5ec92c36a2517",
    	role: "user",
    },
    {
        ime: "batman",
        priimek: "batman",
        uporabniskoIme: "batman",
        email: "batman@gmail.com",
        nakljucnaVrednost: "a6895543c5dab5a1ee245f6cb8eaff8c",
        geslo: "867e141eda0838b7d79084a3a46ac1cf4efdd77495266c9edd177ab8be1b41c886f4d5c12eb96c170d71e74d77b7944c9d59b929b143bc0eb90fcd3ba2c7f0fd",
    	role: "admin",
    }
];

var viriData = [
	{
		naziv: "24 UR",
		url: "https://www.24ur.com/rss",
		opis: "test",
		ikona: "https://www.24ur.com/favicon.ico",
		novice: []
	},

	{
		naziv: "BBC news",
		url: "http://feeds.bbci.co.uk/news/rss.xml?edition=uk",
		opis: "neki neki",
		ikona: "https://www.bbc.com/favicon.ico",
		novice: []
	},

	{
		naziv: "Rtv-SLO",
		url: "https://www.rtvslo.si/feeds/00.xml",
		opis: "neki neki",
		ikona: "https://www.rtvslo.si/favicon.ico",
		novice: []
	},

	{
		naziv: "Siol novice",
		url: "https://siol.net/feeds/section/novice",
		opis: "neki neki",
		ikona: "https://siol.net/favicon.ico",
		novice: []
	},

	{
		naziv: "Fri garaža",
		url: "https://garaza.io/index.xml",
		opis: "neki neki",
		ikona: "https://garaza.io/favicon.ico",
		novice: []
	},

	{
		naziv: "Fox news",
		url: "http://feeds.foxnews.com/foxnews/scitech",
		opis: "neki neki",
		ikona: "https://static.foxnews.com/static/orion/styles/img/fox-news/favicons/favicon-32x32.png",
		novice: []
	},

	{
		naziv: "CNN news",
		url: "http://rss.cnn.com/rss/cnn_topstories.rss",
		opis: "neki neki",
		ikona: "https://edition.cnn.com/favicon.ico",
		novice: []
	},

	{
		naziv: "BBC sports",
		url: "http://feeds.bbci.co.uk/sport/rss.xml?edition=int",
		opis: "neki neki",
		ikona: "https://www.bbc.com/favicon.ico",
		novice: []
	},

	{
		naziv: "NBA",
		url: "http://www.nba.com/rss/nba_rss.xml",
		opis: "neki neki",
		ikona: "http://www.nba.com/favicon.ico",
		novice: []
	},

	{
		naziv: "Wired",
		url: "http://feeds.wired.com/wired/index",
		opis: "neki neki",
		ikona: "https://www.wired.com/favicon.ico",
		novice: []
	},

	{
		naziv: "PC World",
		url: "http://feeds.pcworld.com/pcworld/latestnews",
		opis: "neki neki",
		ikona: "https://csmb.staticworld.net/images/furniture/pcworld/pcw-apple-touch-icon-precomposed-72.png",
		novice: []
	},

	{
		naziv: "MAC World",
		url: "http://rss.macworld.com/macworld/feeds/main",
		opis: "neki neki",
		ikona: "https://www.macworld.com/www.idgcsmb.mw/favicon.ico",
		novice: []
	},

	{
		naziv: "Techworld",
		url: "http://www.techworld.com/news/rss",
		opis: "neki neki",
		ikona: "http://www.techworld.com/favicon.ico",
		novice: []
	},
	{
		naziv: "The West Australian",
		url: "https://thewest.com.au/rss",
		opis: "neki neki",
		ikona: "https://thewest.com.au/favicon.ico",
		novice: []
	},
	{
		naziv: "Računalniške novice",
		url: "https://www.racunalniske-novice.com/rss-novice/",
		opis: "neki neki",
		ikona: "https://www.racunalniske-novice.com/favicon.ico",
		novice: []
	},
	{
		naziv: "New York Times",
		url: "https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/world/rss.xml",
		opis: "neki neki",
		ikona: "https://www.nytimes.com/favicon.ico",
		novice: []
	},
	{
		naziv: "Buzzfeed",
		url: "https://www.buzzfeed.com/world.xml",
		opis: "neki neki",
		ikona: "https://www.buzzfeed.com/favicon.ico",
		novice: []
	},
	{
		naziv: "Aljazeera",
		url: "https://www.aljazeera.com/xml/rss/all.xml",
		opis: "neki neki",
		ikona: "https://www.aljazeera.com/favicon.ico",
		novice: []
	},
	{
		naziv: "Defence Blog",
		url: "http://defence-blog.com/feed",
		opis: "neki neki",
		ikona: "http://defence-blog.com/favicon.ico",
		novice: []
	},
	{
		naziv: "The Cipher Brief",
		url: "https://www.thecipherbrief.com/feed",
		opis: "neki neki",
		ikona: "https://www.thecipherbrief.com/favicon.ico",
		novice: []
	},
	{
		naziv: "The Guardian",
		url: "https://www.theguardian.com/world/rss",
		opis: "neki neki",
		ikona: "https://www.theguardian.com/favicon.ico",
		novice: []
	},
	{
		naziv: "Sputnik News",
		url: "https://sputniknews.com/export/rss2/world/index.xml",
		opis: "neki neki",
		ikona: "https://sputniknews.com/favicon.ico",
		novice: []
	},
	{
		naziv: "Radio France",
		url: "http://en.rfi.fr/general/rss",
		opis: "neki neki",
		ikona: "http://en.rfi.fr/favicon.ico",
		novice: []
	},
	{
		naziv: "Delo",
		url: "http://www.delo.si/rss/",
		opis: "neki neki",
		ikona: "http://www.delo.si/favicon.ico",
		novice: []
	},
	{
		naziv: "ABC News: Top Stories",
		url: "https://abcnews.go.com/abcnews/topstories",
		opis: "neki neki",
		ikona: "https://abcnews.go.com/favicon.ico",
		novice: []
	},
	{
		naziv: "ABC News: Technology",
		url: "https://abcnews.go.com/abcnews/technologyheadlines",
		opis: "neki neki",
		ikona: "https://abcnews.go.com/favicon.ico",
		novice: []
	},
	{
		naziv: "xkcd",
		url: "https://xkcd.com/rss.xml",
		opis: "neki neki",
		ikona: "https://xkcd.com/favicon.ico",
		novice: []
	},
	{
		naziv: "CSS tricks",
		url: "https://css-tricks.com/feed/",
		opis: "neki neki",
		ikona: "https://css-tricks.com/favicon.ico",
		novice: []
	},
	{
		naziv: "The Local Spain",
		url: "https://www.thelocal.es/feeds/rss.php",
		opis: "neki neki",
		ikona: "https://www.thelocal.es/favicon.ico",
		novice: []
	},
	{
		naziv: "The Sun",
		url: "https://www.thesun.co.uk/news/worldnews/feed/",
		opis: "neki neki",
		ikona: "https://www.thesun.co.uk/favicon.ico",
		novice: []
	}
];