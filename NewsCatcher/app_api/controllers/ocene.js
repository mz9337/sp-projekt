var mongoose = require('mongoose');
var Vir = mongoose.model('Vir');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.oceneSeznam = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idVira) {
		Vir
			.findById(zahteva.params.idVira)
			.select('ocene')
			.exec(function(napaka, vir) {
				if (napaka) {
	           		vrniJsonOdgovor(odgovor, 500, napaka);
	          	   	return;
	           	}
	           	
	           	vrniJsonOdgovor(odgovor, 200, vir.ocene);
			});
	} else {
		vrniJsonOdgovor(odgovor, 400, { 
	    	"sporočilo": "Manjka enolični identifikator idVira!"
	    });
	}
};

module.exports.oceneKreiraj = function(zahteva, odgovor) {
	var idVira = zahteva.params.idVira;
	if (idVira) {
		Vir
			.findById(idVira)
			.select('ocene')
			.exec(
				function(napaka, vir) {
					if (napaka) {
				    	vrniJsonOdgovor(odgovor, 400, napaka);
				  	} else {
				    	if (!vir) {
							vrniJsonOdgovor(odgovor, 404, {
							  "sporočilo": "Ne najdem vira."
							});
						} else {
							Uporabnik
								.findById(zahteva.body.idOsebe)
								.exec(function(napaka, uporabnik) {
									if (napaka) {
				    					vrniJsonOdgovor(odgovor, 400, napaka);
				    				} else {
				    					if (!uporabnik) {
				    						vrniJsonOdgovor(odgovor, 404, {
												"sporočilo": "Ne najdem uporabnika."
											});
				    					} else {
				    						var ocena = null;

								        	vir.ocene.forEach(function(ocenaTmp) {
												if(ocenaTmp.idOsebe == zahteva.body.idOsebe) {
													ocena = ocenaTmp; 
													return; 
												}
											});

											if (ocena != null) {
												vrniJsonOdgovor(odgovor, 404, {
													"sporočilo": "Ocena tega uporabnika že obstaja."
												});
											} else {
												vir.ocene.push({
													idOsebe: zahteva.body.idOsebe,
													ocena: zahteva.body.ocena,
												});
												vir.save(function(napaka, vir) {
													var dodanaOcena;
													if (napaka) {
														vrniJsonOdgovor(odgovor, 400, napaka);
													} else {
														dodanaOcena = vir.ocene[vir.ocene.length - 1];
														vrniJsonOdgovor(odgovor, 201, dodanaOcena);
													}
												});
											}
											
				    					}
				    				}
								});
						}
				  	}
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem vira, idVira je obvezen parameter."
		});
	}
};

module.exports.ocenePreberi = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idVira && zahteva.params.idUporabnika) {
		Vir
			.findById(zahteva.params.idVira)
			.select('ocene')
			.exec(
				function(napaka, vir) {
					if (!vir) {
						vrniJsonOdgovor(odgovor, 404, {
						  	"sporočilo": "Ne najdem vira s podanim enoličnim identifikatorjem idVira."
						});
						return;
					} else if (napaka) {
						vrniJsonOdgovor(odgovor, 500, napaka);
						return;
					}
					
					var ocena = null;
					
					if (vir.ocene && vir.ocene.length > 0) {
						// novica = vir.ocene.id(zahteva.params.idocene);

						vir.ocene.forEach(function(ocenaTmp) {
							if(ocenaTmp.idOsebe == zahteva.params.idUporabnika) {
								ocena = ocenaTmp; 
								return; 
							}
						});

						if (ocena == null) {
							vrniJsonOdgovor(odgovor, 404, {
								"sporočilo": "Ne najdem ocene za podanega uporabnika."
							});
						} else {
						 	vrniJsonOdgovor(odgovor, 200, {
							    ocena: ocena
						  	});
						}
					} else {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Ne najdem nobene ocene."
						});
					}
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem zapisa, oba enolična identifikatorja idVira in idUporabnika sta zahtevana."
		});
	}
};

module.exports.ocenePosodobi = function(zahteva, odgovor) {
	if (!zahteva.params.idVira || !zahteva.params.idUporabnika) {
	    vrniJsonOdgovor(odgovor, 400, {
	    	"sporočilo": "Ne najdem vira oz. ocene, idVira in idUporabnika sta obvezna parametra."
	    });

	    return;
	}
  	
  	Vir
	    .findById(zahteva.params.idVira)
	    .select('ocene')
	    .exec(
	      	function(napaka, vir) {
		        if (!vir) {
		          	vrniJsonOdgovor(odgovor, 404, {
		            	"sporočilo": "Ne najdem vira."
		         	});
		          	return;
		        } else if (napaka) {
		          	vrniJsonOdgovor(odgovor, 500, napaka);
		          	return;
		        }

		        if (vir.ocene && vir.ocene.length > 0) {
		        	var ocena = null;

		        	vir.ocene.forEach(function(ocenaTmp) {
						if(ocenaTmp.idOsebe == zahteva.params.idUporabnika) {
							ocena = ocenaTmp; 
							return; 
						}
					});

					if (ocena == null) {
						vrniJsonOdgovor(odgovor, 404, {
					  		"sporočilo": "ocene ne najdem."
						});
					} else {
						ocena.ocena = (typeof zahteva.body.ocena !== "undefined") ? zahteva.body.ocena : ocena.ocena;

						vir.save(function(napaka, vir) {
							if (napaka) {
						    	vrniJsonOdgovor(odgovor, 400, napaka);
						  	} else {
						    	vrniJsonOdgovor(odgovor, 200, ocena);
						  	}
						});
					}
		        } else {
		        	vrniJsonOdgovor(odgovor, 404, {
		            	"sporočilo": "Ni ocen za ažuriranje."
		          	});
		        }
	      	}
	    );
};

module.exports.oceneIzbrisi = function(zahteva, odgovor) {

	if (!zahteva.params.idVira || !zahteva.params.idUporabnika) {
		vrniJsonOdgovor(odgovor, 400, {
		  "sporočilo": "Ne najdem vira oz. uporabnika, idVira in idUporabnika sta obvezna parametra."
		});

		return;
	}

	Vir
		.findById(zahteva.params.idVira)
		.exec(
			function(napaka, vir) {
				if (!vir) {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "Vira ne najdem."
				  	});
				  	return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				if (vir.ocene && vir.ocene.length > 0) {
					var ocena = null;

		        	vir.ocene.forEach(function(ocenaTmp) {
						if(ocenaTmp.idOsebe == zahteva.params.idUporabnika) {
							ocena = ocenaTmp; 
							return; 
						}
					});

					if (ocena == null) {
						vrniJsonOdgovor(odgovor, 404, {
					  		"sporočilo": "Ocene ne najdem."
						});
					} else {
						var ocenaIndex = vir.ocene.indexOf(ocena); 
						vir.ocene.splice(ocenaIndex, 1);
						vir.save(function(napaka) {
							if (napaka) {
								vrniJsonOdgovor(odgovor, 500, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, { "status": "uspešno" });
							}
						});
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "Ni ocene za brisanje."
				  	});
				}
			}
		);
};