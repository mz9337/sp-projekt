var mongoose = require('mongoose');
var Novica = mongoose.model('Novica');
var Vir = mongoose.model('Vir');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

var dodajNovico = function(zahteva, odgovor, vir) {
	if (!vir) {
		vrniJsonOdgovor(odgovor, 404, {
		  "sporočilo": "Ne najdem vira."
		});
	} else {
		vir.novice.push({
			naziv: zahteva.body.naziv,
			opis: zahteva.body.opis,
			url: zahteva.body.url,
			datumObjave: zahteva.body.datumObjave,
			slika: zahteva.body.slika
		});
		vir.save(function(napaka, vir) {
			var dodanaNovica;
			if (napaka) {
				vrniJsonOdgovor(odgovor, 400, napaka);
			} else {
				dodanaNovica = vir.novice[vir.novice.length - 1];
				vrniJsonOdgovor(odgovor, 201, dodanaNovica);
			}
		});
	}
};

module.exports.noviceSeznam = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idVira) {
		Vir
			.findById(zahteva.params.idVira)
			.select('novice')
			.exec(function(napaka, novice) {
				if (napaka) {
	           		vrniJsonOdgovor(odgovor, 500, napaka);
	          	   	return;
	           	}

	           	var novice = novice.novice;
	           	novice.sort(function(a,b){
					return new Date(b.datumObjave) - new Date(a.datumObjave);
				});
	           	
	           	vrniJsonOdgovor(odgovor, 200, novice);
			});
	} else {
		vrniJsonOdgovor(odgovor, 400, { 
	    	"sporočilo": "Manjka enolični identifikator idVira!"
	    });
	}
};

module.exports.noviceKreiraj = function(zahteva, odgovor) {
	var idVira = zahteva.params.idVira;
	if (idVira) {
		Vir
			.findById(idVira)
			.select('novice')
			.exec(function(napaka, vir) {
				if (napaka) {
			    	vrniJsonOdgovor(odgovor, 400, napaka);
			  	} else {
					Vir
						.find( { 'novice.naziv': zahteva.body.naziv } )
						.exec(function(napaka, novica) {
							if (!novica || novica.length == 0) {
			    				dodajNovico(zahteva, odgovor, vir);
							} else {
								vrniJsonOdgovor(odgovor, 400, {
									"sporočilo": "Novica že obstaja."
								});
							}
						});
			  	}
			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem vira, idVira je obvezen parameter."
		});
	}
};

module.exports.novicePreberi = function(zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idVira && zahteva.params.idNovice) {
		Vir
			.findById(zahteva.params.idVira)
			.select('novice')
			.exec(
				function(napaka, vir) {
					var novica;
					if (!vir) {
						vrniJsonOdgovor(odgovor, 404, {
						  	"sporočilo": "Ne najdem vira s podanim enoličnim identifikatorjem idVira."
						});
						return;
					} else if (napaka) {
						vrniJsonOdgovor(odgovor, 500, napaka);
						return;
					}
					if (vir.novice && vir.novice.length > 0) {
						novica = vir.novice.id(zahteva.params.idNovice);
						if (!novica) {
							vrniJsonOdgovor(odgovor, 404, {
								"sporočilo": "Ne najdem novice s podanim enoličnim identifikatorjem idNovice."
							});
						} else {
						 	vrniJsonOdgovor(odgovor, 200, {
							    novica: novica
						  	});
						}
					} else {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Ne najdem nobene novice."
						});
					}
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Ne najdem zapisa, oba enolična identifikatorja idVira in idNovice sta zahtevana."
		});
	}
};

module.exports.novicePosodobi = function(zahteva, odgovor) {
	if (!zahteva.params.idVira || !zahteva.params.idNovice) {
	    vrniJsonOdgovor(odgovor, 400, {
	    	"sporočilo": "Ne najdem vira oz. novice, idVira in idNovice sta obvezna parametra."
	    });

	    return;
	}
  	
  	Vir
	    .findById(zahteva.params.idVira)
	    .select('novice')
	    .exec(
	      	function(napaka, vir) {
		        if (!vir) {
		          	vrniJsonOdgovor(odgovor, 404, {
		            	"sporočilo": "Ne najdem vira."
		         	});
		          	return;
		        } else if (napaka) {
		          	vrniJsonOdgovor(odgovor, 500, napaka);
		          	return;
		        }

		        if (vir.novice && vir.novice.length > 0) {
		        	var trenutnaNovica = vir.novice.id(zahteva.params.idNovice);
					if (!trenutnaNovica) {
						vrniJsonOdgovor(odgovor, 404, {
					  		"sporočilo": "Novice ne najdem."
						});
					} else {
						trenutnaNovica.naziv = (typeof zahteva.body.naziv !== "undefined") ? zahteva.body.naziv : trenutnaNovica.naziv;
						trenutnaNovica.opis = (typeof zahteva.body.opis !== "undefined") ? zahteva.body.opis : trenutnaNovica.opis;
						trenutnaNovica.url = (typeof zahteva.body.url !== "undefined") ? zahteva.body.url : trenutnaNovica.url;
						trenutnaNovica.datumObjave = (typeof zahteva.body.datumObjave !== "undefined") ? zahteva.body.datumObjave : trenutnaNovica.datumObjave;
						trenutnaNovica.slika = (typeof zahteva.body.slika !== "undefined") ? zahteva.body.slika : trenutnaNovica.slika;

						vir.save(function(napaka, vir) {
							if (napaka) {
						    	vrniJsonOdgovor(odgovor, 400, napaka);
						  	} else {
						    	vrniJsonOdgovor(odgovor, 200, trenutnaNovica);
						  	}
						});
					}
		        } else {
		        	vrniJsonOdgovor(odgovor, 404, {
		            	"sporočilo": "Ni novic za ažuriranje."
		          	});
		        }
	      	}
	    );
};

module.exports.noviceIzbrisi = function(zahteva, odgovor) {

	if (!zahteva.params.idVira || !zahteva.params.idNovice) {
		vrniJsonOdgovor(odgovor, 400, {
		  "sporočilo": "Ne najdem vira oz. novice, idVira in idNovice sta obvezna parametra."
		});

		return;
	}

	Vir
		.findById(zahteva.params.idVira)
		.exec(
			function(napaka, vir) {
				if (!vir) {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "Vira ne najdem."
				  	});
				  	return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				if (vir.novice && vir.novice.length > 0) {
					if (!vir.novice.id(zahteva.params.idNovice)) {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Novice ne najdem."
						});
					} else {
						vir.novice.id(zahteva.params.idNovice).remove();
						vir.save(function(napaka) {
							if (napaka) {
								vrniJsonOdgovor(odgovor, 500, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, { "status": "uspešno" });
							}
						});
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
				    	"sporočilo": "Ni novice za brisanje."
				  	});
				}
			}
		);
};