var passport = require('passport');
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.registracija = function(zahteva, odgovor) {
	if (!zahteva.body.ime || !zahteva.body.priimek || !zahteva.body.email || !zahteva.body.geslo) {
		vrniJsonOdgovor(odgovor, 400, {
			"sporočilo": "Zahtevani so vsi podatki"
		});
	} else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(zahteva.body.email))) {
	    vrniJsonOdgovor(odgovor, 400, {
	      "sporočilo": "Elektronski naslov je neustrezen!"
	    });

	    return;
	}

	var uporabnik = new Uporabnik();
	uporabnik.ime = zahteva.body.ime;
	uporabnik.priimek = zahteva.body.priimek;
	uporabnik.uporabniskoIme = zahteva.body.uporabniskoIme;
	uporabnik.email = zahteva.body.email;
	uporabnik.nastaviGeslo(zahteva.body.geslo);

	uporabnik.save(function(napaka) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 500, napaka);
		} else {
		 	vrniJsonOdgovor(odgovor, 200, {
				"zeton": uporabnik.generirajJwt()
		 	});
		}
	});
};

module.exports.prijava = function(zahteva, odgovor) {
	if (!zahteva.body.email || !zahteva.body.geslo) {
    	vrniJsonOdgovor(odgovor, 400, {
     		"sporočilo": "Zahtevani so vsi podatki"
    	});
  	} else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(zahteva.body.email))) {
	    vrniJsonOdgovor(odgovor, 400, {
	      "sporočilo": "Elektronski naslov je neustrezen!"
	    });

	    return;
	}
  	
	passport.authenticate('local', function(napaka, uporabnik, podatki) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 404, napaka);
		  	return;
		}
		if (uporabnik) {
		  	vrniJsonOdgovor(odgovor, 200, {
		    	"zeton": uporabnik.generirajJwt()
		  	});
		} else {
		  	vrniJsonOdgovor(odgovor, 401, podatki);
		}
	})(zahteva, odgovor);
};