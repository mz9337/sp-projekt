var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

ObjectId = mongoose.Schema.ObjectId;

var uporabnikiShema = new mongoose.Schema({
  	ime: {type: String, required: true},
	priimek: {type: String, required: true},
	uporabniskoIme: {type: String, required: true},
	email: String,
	geslo: {type: String, required: true},
	nakljucnaVrednost: String,
	viri: [{ type: ObjectId, ref: 'Vir' }],
	role: String
});

uporabnikiShema.methods.nastaviGeslo = function(geslo) {
	this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
	this.geslo = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

uporabnikiShema.methods.preveriGeslo = function(geslo) {
	var novoGeslo = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
	return this.geslo == novoGeslo;
}

uporabnikiShema.methods.generirajJwt = function() {
	var datumPoteka = new Date();
	datumPoteka.setDate(datumPoteka.getDate() + 7);

	return jwt.sign({
		_id: this._id,
		email: this.email,
		ime: this.ime,
		datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10),
		role: this.role
	}, process.env.JWT_GESLO);
}

uporabnikiShema.plugin(mongoosePaginate);
mongoose.model('Uporabnik', uporabnikiShema, 'Uporabniki');