var mongoose = require('mongoose');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

ObjectId = mongoose.Schema.ObjectId;

var noviceShema = new mongoose.Schema({
  	naziv: {type: String, required: true},
	opis: String,
	url: {type: String, required: true},
	datumObjave: Date,
	slika: String
});

var oceneShema = new mongoose.Schema({
  	idOsebe: {type: ObjectId, required: true},
	ocena: {type: Number, required: true},
});

var viriShema = new mongoose.Schema({
  	naziv: {type: String, required: true},
	url: {type: String, required: true},
	opis: String,
	ikona: String,
	novice: [noviceShema],
	ocene: [oceneShema]
});

viriShema.plugin(mongooseAggregatePaginate);
mongoose.model('Vir', viriShema, 'Viri');