var mongoose = require('mongoose');

var noviceShema = new mongoose.Schema({
  	naziv: {type: String, required: true},
	opis: String,
	url: {type: String, required: true},
	datumObjave: Date,
	slika: String
});

mongoose.model('Novica', noviceShema, 'Novice');