(function() {
    function adminController(viri, $timeout, $uibModal) {
        var vm = this;

        vm.naStran = 10;
        vm.trenutnaStran = 1;
        vm.steviloStrani = 1;
        vm.isciVir = '';
        vm.napaka = "";
        vm.uspesno = "";
        vm.oknoPosodabljanje;

        var prvic = true;
        vm.steviloVirov = 1;

        vm.viriFilter = function(vir) {
            if (vm.steviloVirov > vm.naStran) 
                return true;

            return (vir.naziv.toLowerCase().indexOf(vm.isciVir.toLowerCase()) !== -1)
        }

        vm.vrniVire = function(stran, naStran, idUporabnika) {
            vm.trenutnaStran = stran > 0 ? stran : 1;
            vm.trenutnaStran =  vm.trenutnaStran < vm.steviloStrani ? vm.trenutnaStran : vm.steviloStrani;

            viri.vrniVire(vm.trenutnaStran, naStran, vm.isciVir).then(
                function success(odgovor) {
                    vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
                    vm.viri = odgovor.data.viri;
                    vm.steviloStrani = odgovor.data.steviloStrani;
                    if (prvic) {
                        vm.steviloVirov = odgovor.data.steviloVirov;
                        prvic = false;
                    }
                }, function error(odgovor) {
                    vm.napaka = "Prišlo je do napake!";
                    vm.uspesno = "";
                }
            );
        };

        vm.vrniVire(1, vm.naStran, -1);

        vm.izbrisiVir = function(idVira, stran, naStran, idUporabnika) {
            viri.izbrisiVir(idVira).then(
                function success(odgovor){
                    vm.vrniVire(stran, naStran, idUporabnika);
                    vm.napaka = "";
                    vm.uspesno = "Vir je uspešno odstranjen!";
                }, function error(odgovor) {
                    vm.napaka = "Napaka pri brisanju vira!";
                    vm.uspesno = "";
                }
            )
        };

        vm.prikaziOkno = function() {
            $uibModal.open({
                templateUrl:  'modalnoOkno/modalnoOkno.pogled.html',
                controller:   'modalnoOknoController',
                controllerAs: 'vm'
            });
        };


        vm.prikaziOknoPosodabljanje = function(id, url, naziv, opis) {
            vm.oknoPosodabljanje = $uibModal.open({
                templateUrl:  'modalnoOknoPosodabljanje/modalnoOknoPosodabljanje.pogled.html',
                controller:   'modalnoOknoPosodabljanjeController',
                controllerAs: 'vm',
                resolve: {
                    zbraniVir: function() {
                        return {
                            _id: id,
                            url: url,
                            naziv: naziv,
                            opis: opis
                        };
                    },
                    posodobiCallback: function(){
                        return vm.posodobiCallback;  
                    } 
                }
            });
        };

        vm.posodobiCallback = function(idVira, naziv) {
            vm.viri.forEach(function(vir) {
                if (vir._id == idVira) {
                    vir.naziv = naziv;
                } 
            });
        }
    }

    adminController.$inject = ['viri', '$timeout', '$uibModal'];

    angular
        .module('newsCatcher')
        .controller('adminController', adminController)
})();