(function() {
    function registracijaController(avtentikacija, $location, $timeout) {

        var vm = this;
        
        vm.uporabnik = {
            ime: "",
            priimek: "",
            email: "",
            uporabniskoIme: "",
            geslo: ""
        };  

        vm.registracijaUporabnika = function() {
            var napaka = false;
            vm.napakaNaObrazcu = "";
            vm.napakaPolje = "";
            vm.napakaEmail = "";

            if(!vm.uporabnik.ime){
                vm.napakaPolje = "Zahtevano polje!";
                napaka = true;
            }
            
            if(!vm.uporabnik.priimek){
                vm.napakaPolje = "Zahtevano polje!";
                napaka = true;
            }

            if(!vm.uporabnik.email){
                vm.napakaEmail = "Vnesite email oblike npr. ime@priimek.com";
                napaka = true;
            }

            if(!vm.uporabnik.uporabniskoIme){
                vm.napakaPolje = "Zahtevano polje!";
                napaka = true;
            }

            if(!vm.uporabnik.geslo){
                vm.napakaPolje = "Zahtevano polje!";
                napaka = true;
            }

            if (napaka) {
                vm.napakaNaObrazcu = "Prišlo je do napake pri vsaj enem vnosu!";
                return false;
            } else {
                vm.izvediRegistracijo();
            }
        };

        vm.izvediRegistracijo = function() {
            avtentikacija.registracija(vm.uporabnik).then(
                function(success) {
                    vm.sporocilo = "Registracija je bila uspešna!";
                    vm.sporociloBool = true;
                    $timeout(function() {
                        vm.sporociloBool = false;
                        $location.path('/');
                    }, 1000);
                },
                function(napaka) {
                    vm.napakaNaObrazcu = "Prišlo je do napake pri vsaj enem vnosu!";
                    $location.path('/registracija');
                }
            );
        };
    }    
    registracijaController.$inject = ['avtentikacija', '$location', '$timeout'];

    angular
        .module('newsCatcher')
        .controller('registracijaController', registracijaController);
})();