(function() {
	var avtorizacija = function(avtentikacija, $q) {

		return {
	        authenticate: function(roles){
	            if(avtentikacija.imaRole(roles)){
	                return true;
	            } else {
	                return $q.reject('Not Authenticated');
	            }
	        }
	    }
	};

	avtorizacija.$inject = ['avtentikacija', '$q'];

	angular
		.module('newsCatcher')
		.factory('avtorizacija', avtorizacija);
})();