(function() {
  var navigacija = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/navigacija/navigacija.predloga.html',
      controller: 'navigacijaController',
      controllerAs: 'navvm'
    };
  }
  
  angular
  	.module('newsCatcher')
    .directive('navigacija', navigacija);
})();