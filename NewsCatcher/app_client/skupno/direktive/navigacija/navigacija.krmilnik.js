(function() {
	function navigacijaController($location, $route, avtentikacija) {
		var navvm = this;

		navvm.trenutnaLokacija = $location.path();

		navvm.jePrijavljen = avtentikacija.jePrijavljen();

		navvm.imaRole = function(roles) {
			return avtentikacija.imaRole(roles);
		}
    
	    navvm.trenutniUporabnik = avtentikacija.trenutniUporabnik();

	    navvm.odjava = function() {
			avtentikacija.odjava();
			$location.path('/');
			$route.reload();
	    };
	}

	navigacijaController.$inject = ['$location', '$route', 'avtentikacija'];

	angular
		.module('newsCatcher')
		.controller('navigacijaController', navigacijaController);
})();