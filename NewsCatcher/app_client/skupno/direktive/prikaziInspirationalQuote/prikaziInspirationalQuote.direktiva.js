(function() {
    var prikaziInspirationalQuote = function() {
      return {
        restrict: 'EA',
        templateUrl: '/skupno/direktive/prikaziInspirationalQuote/prikazi-inspirational-quote.html',
        controller: function() {
            TheySaidSo.render({ qod_category : 'inspire', node_id: 'tso_simple_borders' });
        }
      };
    };
    
    angular
      .module('newsCatcher')
      .directive('prikaziInspirationalQuote', prikaziInspirationalQuote);    
})();