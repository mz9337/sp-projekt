(function() {
  var noga = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/direktive/noga/noga.predloga.html'
    };
  };
  
  angular
  	.module('newsCatcher')
    .directive('noga', noga);
})();