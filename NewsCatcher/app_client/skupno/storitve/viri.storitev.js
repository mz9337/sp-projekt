(function() {
	var viri = function($http) {

		var vrniVir = function(idVira) {
			return $http.get('api/viri/'+idVira);
		};

		var vrniVire = function(stran, naStran, isciVir) {
			return $http.get('api/viri?stran='+stran+'&naStran='+naStran+'&isci='+isciVir);
		};

		var vrniMojeVire = function(stran, naStran, isciVir, idUporabnika) {
			return $http.get('api/uporabniki/'+idUporabnika+'/viri?stran='+stran+'&naStran='+naStran+'&isci='+isciVir);
		};

	    var dodajVir = function(vir) {
	        return $http.post('api/viri', vir);
	    };

        var posodobiVir = function(vir) {
            return $http.put('api/viri/'+vir._id, vir);
        };
        
        var dodajMojVir = function(idUporabnika, idVira) {
            return $http.post('api/uporabniki/'+idUporabnika+'/viri', idVira);
        };
        
        var odjaviMojVir = function(idUporabnika, idVira) {
            return $http.delete('api/uporabniki/'+idUporabnika+'/viri/'+idVira);
        };

        var izbrisiVir = function (idVira) {
			return $http.delete('api/viri/'+idVira);
        };

        var oceniNovico = function(idUporabnika, idVira, params) {
            return $http.put('api/viri/'+idVira+'/ocene/'+idUporabnika, params);
        }
		
		var dodajOcenoNovice = function(idVira, params) {
			return $http.post('api/viri/'+idVira+'/ocene', params);
		}
		
		var pridobiOcenoUporabnika = function(idUporabnika, idVira) {
			return $http.get('api/viri/'+idVira+'/ocene/'+idUporabnika);
		}

		return {
			vrniVir: vrniVir,
			vrniVire: vrniVire,
			vrniMojeVire: vrniMojeVire,
	        dodajVir: dodajVir,
            posodobiVir: posodobiVir,
            dodajMojVir: dodajMojVir,
            odjaviMojVir: odjaviMojVir,
            izbrisiVir: izbrisiVir,
			oceniNovico: oceniNovico,
			dodajOcenoNovice: dodajOcenoNovice,
			pridobiOcenoUporabnika: pridobiOcenoUporabnika
	    }
	};

	viri.$inject = ['$http'];

	angular
		.module('newsCatcher')
		.service('viri', viri);
})();