(function() {
    var uporabniki = function($http) {
        var vrniUporabnika = function(id_uporabnik) {
            return $http.get('api/uporabniki/' + id_uporabnik);
        };

        var posodobiUporabnika = function(id_uporabnika, uporabnik) {
            return $http.put('api/uporabniki/'+id_uporabnika, uporabnik);
        };

        return {
            vrniUporabnika: vrniUporabnika,
            posodobiUporabnika: posodobiUporabnika
        };
    };
    uporabniki.$inject = ['$http'];

    angular
        .module('newsCatcher')
        .service('uporabniki', uporabniki);
})();