(function() {
	var db = function($http, $timeout, $rootScope) {
		var ustvariPodatke = function() {
			return $http.post('api/db/ustvari');
		};

		var izbrisiPodatke = function(idVira) {
			return $http.post('api/db/izbrisi');
		};

		var osveziRSS = function() {
			$timeout(function() {
                $rootScope.uspesnoDodanBool = false;
            }, 5000);
			return $http.post('api/db/rss');
		}

		return {
			ustvariPodatke: ustvariPodatke,
			izbrisiPodatke: izbrisiPodatke,
			osveziRSS: osveziRSS
		};
	};

	db.$inject = ['$http', '$timeout', '$rootScope'];

	angular
		.module('newsCatcher')
		.service('db', db);
})();