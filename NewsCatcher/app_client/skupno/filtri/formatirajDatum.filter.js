(function() {
  var formatirajDatum = function() {
    return function(datum) {
        if (typeof datum != undefined) {
            var datum = new Date(datum);
            var imenaMesecev = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ];
            var d = datum.getDate();
            var m = imenaMesecev[datum.getMonth()];
            var l = datum.getFullYear();
            var odgovor = d + ". " + m + ". " + l;

            return odgovor;
        } else {
            return "Preveri vhodni parameter."
        }
    };
  };
  
  angular
    .module('newsCatcher')
    .filter('formatirajDatum', formatirajDatum);
})();