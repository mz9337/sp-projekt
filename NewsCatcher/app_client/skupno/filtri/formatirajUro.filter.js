(function() {
  var formatirajUro = function() {
    var dodajZacetnoNiclo = function(steviloString) {
        if (typeof(steviloString) == "string" && steviloString.length == 1) {
            steviloString = "0" + steviloString;
        }
        return steviloString;
    }
      
    return function(datum) {
        if (typeof datum != undefined) {
            var datum = new Date(datum);
            var ure = dodajZacetnoNiclo(datum.getHours().toString());
            var minute = dodajZacetnoNiclo(datum.getMinutes().toString());
            var odgovor = ure + ":" + minute;
            
            return odgovor;
        } else {
            return "Preveri vhodni parameter."
        }
    };
  };
  
  angular
    .module('newsCatcher')
    .filter('formatirajUro', formatirajUro);
})();