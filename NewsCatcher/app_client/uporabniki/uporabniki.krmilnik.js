(function() {
    function uporabnikiController(uporabniki, avtentikacija, $location) {

        var vm = this;

        uporabniki.vrniUporabnika(avtentikacija.trenutniUporabnik().id).then(
            function success(odgovor) {
                vm.napakaNaObrazcu = "";
                if(odgovor.data.ime !== undefined && odgovor.data.priimek !== undefined &&
                    odgovor.data.email !== undefined && odgovor.data.uporabniskoIme !== undefined)
                {
                    vm.uporabnik = odgovor.data;
                    vm.uporabnik.geslo = "";
                    vm.sporocilo = "";
                } else {
                    vm.napakaNaObrazcu = "Prišlo je do napake!";
                }
            }, function error(odgovor) {
                vm.napakaNaObrazcu = "Prislo je do napake!";
            }
        );

        vm.posodobiUporabnika = function() {
            vm.napakaNaObrazcu = "";
            vm.uspesnoPosodobljen = "";
            vm.napakaIme = "";
            vm.napakaPriimek = "";
            vm.napakaUporabniskoIme = "";
            vm.napakageslo = "";
            var napaka = false;

            if (!vm.uporabnik.ime) {
                vm.napakaIme = "Ime lahko vsebuje velike in male črke, piko in presledek! Minimalna dolžina imena je 2.";
                napaka = true;
            }

            if (!vm.uporabnik.priimek) {
                vm.napakaPriimek = "Priimek lahko vsebuje velike male črke, piko in presledek! Minimalna dolžina 2";
                napaka = true;
            }

            if (!vm.uporabnik.uporabniskoIme) {
                vm.napakaUporabniskoIme = "Uporabnisko lahko vsebuje velike male črke, piko in presledek! Minimalna dolžina 2";
                napaka = true;
            }

            if (!vm.uporabnik.geslo) {
                vm.napakageslo = "Geslo lahko vsebuje velike male črke in števila, minimalne dolžine 2!";
                napaka = true;
            }

            if(napaka){
                vm.napakaNaObrazcu = "Prišlo je do napake!";
                return false;
            } else {
                uporabniki.posodobiUporabnika(avtentikacija.trenutniUporabnik().id, vm.uporabnik).then(
                    function (success) {
                        vm.uspesnoPosodobljen = "Uporabniški podatki so uspešno posodobljeni!";
                    },
                    function (error) {
                        vm.napakaNaObrazcu = "Prišlo je do napake!";
                    }
                )
            }
        }
    }
    uporabnikiController.$inject = ['uporabniki', 'avtentikacija', '$location'];

    angular
        .module('newsCatcher')
        .controller('uporabnikiController', uporabnikiController);
})();