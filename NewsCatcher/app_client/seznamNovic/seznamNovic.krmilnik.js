(function() {
	function seznamNovicController(viri, avtentikacija, $routeParams) {
		var vm = this;

		var idVira = $routeParams.idVira;

		vm.sporocilo = "Pridobivam novice.";
		vm.jePrijavljen = avtentikacija.jePrijavljen();

		var naloziVire = function(reloadAll) {
			viri.vrniVir(idVira).then(
				function success(odgovor) {
					vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem vira.";
					if (reloadAll) {
						vm.vir = odgovor.data;
					} else {
						vm.vir.ocene = odgovor.data.ocene;
						vm.vir.ocena = odgovor.data.ocena;
					}
					
				}, function error(odgovor) {
					vm.sporocilo = "Prišlo je do napake!";
					console.log(odgovor.e);
				}
			);
		}

		naloziVire(true);

		if (vm.jePrijavljen) {
			vm.oddajRating = function oddajRating(idVira) {
				viri.oceniNovico(avtentikacija.trenutniUporabnik().id, idVira, {ocena: vm.ocena}).then(
					function success(odgovor) {
						vm.sporocilo = "Uspešno ocenjen vir novic!";
						vm.ocenaUporabnika = vm.ocena;
						naloziVire(false);
					}, function error(odgovor) {
						vm.sporocilo = "Ne najdem ocene za to novico, dodajam novico.";
						viri.dodajOcenoNovice(idVira, {idOsebe: avtentikacija.trenutniUporabnik().id, 
							ocena: vm.ocena}).then(
							function success(odgovor) {
								vm.sporocilo = "Ocena uspešno dodana";
								vm.ocenaUporabnika = vm.ocena;
								naloziVire(false);
							}, function error(odgovor) {
								vm.sporocilo = "Napaka!";
							}
						);
					}
				);
			}

			var pridobiOceno = function pridobiOceno(idVira) {
				viri.pridobiOcenoUporabnika(avtentikacija.trenutniUporabnik().id, idVira).then(
					function success(odgovor) {
						vm.sporocilo = "Uspešno pridobljena ocena uporabnika!";
						vm.ocenaUporabnika = odgovor.data.ocena.ocena;
						return odgovor.data.ocena.ocena;
					}, function error(odgovor) {
						vm.sporocilo = "Ocene uporabnika ne najdem, nastavim jo na 0.";
						vm.ocenaUporabnika = 0;
						return 0;
					}
				)
			}

			vm.ocenaUporabnika = (vm.ocenaUporabnika == null) ? pridobiOceno($routeParams.idVira) : vm.ocenaUporabnika;
		}
	}

	seznamNovicController.$inject = ['viri', 'avtentikacija', '$routeParams'];

	angular
		.module('newsCatcher')
		.controller('seznamNovicController', seznamNovicController);
})();