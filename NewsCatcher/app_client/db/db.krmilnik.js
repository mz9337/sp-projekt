(function() {
    function dbController(db) {
   
        var vm = this;

        vm.sporocilo = "";

        vm.ustvariPodatke = function() {
            vm.sporocilo = "Ustvarjam podatke...";

            db.ustvariPodatke().then(
                function(odgovor) {
                    vm.sporocilo = "Podatki uspešno ustvarjeni!";
                },
                function(odgovor) {
                    vm.sporocilo = "Pri ustvarjanju podatakov je prišlo do napake..."
                    console.log(odgovor.e);
                }
            );
        };

        vm.izbrisiPodatke = function() {
            vm.sporocilo = "Brišem podatke...";

            db.izbrisiPodatke().then(
                function(odgovor) {
                    vm.sporocilo = "Podatki uspešno izbrisani!";
                },
                function(odgovor) {
                    vm.sporocilo = "Pri brisanju podatkov je prišlo do napake..."
                    console.log(odgovor.e);
                }
            );
        };
    }

    dbController.$inject = ['db'];

    angular
        .module('newsCatcher')
        .controller('dbController', dbController)
})();