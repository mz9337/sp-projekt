(function() {
    function mojiViriController(viri, avtentikacija, $window, $uibModal, $route, $location) {
        var vm = this;

        if (avtentikacija.imaRole(['admin'])) {
            $location.path('/admin');
            // $route.reload();
        };

        vm.sporocilo = "Pridobivam vire.";

        vm.jePrijavljen = avtentikacija.jePrijavljen();

        vm.naStran = 10;
        vm.trenutnaStran = -1;
        vm.steviloStrani = 1;
        vm.isciVir = '';

        var prvic = true;
        vm.steviloVirov = 1;

        vm.viriFilter = function(vir) {
            if (vm.steviloVirov > vm.naStran) 
                return true;

            return (vir.naziv.toLowerCase().indexOf(vm.isciVir.toLowerCase()) !== -1)
        }

        vm.vrniVire = function(stran, naStran, idUporabnika) {

            vm.prejsnaStran = vm.trenutnaStran;

            vm.trenutnaStran = stran > 0 ? stran : 1;
            vm.trenutnaStran =  vm.trenutnaStran < vm.steviloStrani ? vm.trenutnaStran : vm.steviloStrani;

            if (idUporabnika == -1) {
                viri.vrniVire(vm.trenutnaStran, naStran, vm.isciVir).then(
                    function success(odgovor) {
                        vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
                        vm.viri = odgovor.data.viri;
                        vm.steviloStrani = odgovor.data.steviloStrani;
                        if (prvic) {
                            vm.steviloVirov = odgovor.data.steviloVirov;
                            prvic = false;
                        }
                    }, function error(odgovor) {
                        vm.sporocilo = "Prišlo je do napake!";
                        console.log(odgovor.e);
                    }
                );
            } else {
                vm.trenutnaStran = stran > 0 ? stran : 1;
                vm.trenutnaStran =  vm.trenutnaStran < vm.steviloStrani ? vm.trenutnaStran : vm.steviloStrani;

                viri.vrniMojeVire(vm.trenutnaStran, naStran, vm.isciVir, idUporabnika).then(
                    function success(odgovor) {
                        vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
                        vm.viri = odgovor.data.viri;
                        vm.steviloStrani = odgovor.data.steviloStrani;
                        vm.steviloVirov = odgovor.data.steviloVirov;
                    }, function error(odgovor) {
                        vm.sporocilo = "Prišlo je do napake!";
                        console.log(odgovor.e);
                    }
                );
            }
        }

        if (vm.jePrijavljen) {

            vm.prikaziOkno = function() {
                $uibModal.open({
                    templateUrl:  'modalnoOkno/modalnoOkno.pogled.html',
                    controller:   'modalnoOknoController',
                    controllerAs: 'vm',
                    resolve: {
                        callback: function() {
                            return vm.callback;
                        }
                    }
                });
            };

            vm.idUporabnika = avtentikacija.trenutniUporabnik().id;
            vm.vrniVire(1, vm.naStran, vm.idUporabnika);
            
            function odjavaMojegaVira(idVira, stran, naStran, idUporabnika) {
                viri.odjaviMojVir(avtentikacija.trenutniUporabnik().id, idVira).then(
                    function success(odgovor) {
                            console.log("here");
                            vm.vrniVire(stran, naStran, idUporabnika);
                            console.log("Uspešno odjavljen vir!");
                        }, function error(odgovor) {
                            console.log("Napaka");
                            vm.sporocilo = "Prišlo je do napake!";
                        }          
                );
            }
        
            vm.odjavaMojegaVira = odjavaMojegaVira;

        } else {
            
            vm.sporocilo = "Pridobivam vire.";

            vm.vrniVire(1, vm.naStran, -1);
        }

        vm.callback = function() {
            vm.vrniVire(vm.trenutnaStran, vm.naStran, vm.idUporabnika);
        }   

    }

    mojiViriController.$inject = ['viri', 'avtentikacija', '$window', '$uibModal', '$route', '$location'];

    angular
        .module('newsCatcher')
        .controller('mojiViriController', mojiViriController);
})();