(function() {
	function seznamVirovController(viri) {
		var vm = this;
		
		vm.sporocilo = "Pridobivam vire.";

		viri.vrniVire().then(
		  	function success(odgovor) {
			    vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
			    vm.viri = odgovor.data;
		  	}, function error(odgovor) {
		    	vm.sporocilo = "Prišlo je do napake!";
		    	console.log(odgovor.e);
		  	}
		);
	}

	seznamVirovController.$inject = ['viri'];

	// newsCatcherApp
	angular
		.module('newsCatcher')
		.controller('seznamVirovController', seznamVirovController);
})();