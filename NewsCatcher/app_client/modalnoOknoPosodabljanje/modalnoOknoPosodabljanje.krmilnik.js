(function() {
    function modalnoOknoPosodabljanjeController(viri, avtentikacija, db, $location, $uibModal, $route, $timeout, $rootScope, zbraniVir, $uibModalStack, posodobiCallback) {
        var vm = this;

        vm.vir = zbraniVir;

        // vm.zapri = zapriOkno;
        
        vm.posiljanjePodatkov = function() {
            var napaka = false;
            vm.napakaNaObrazcu = "";
            vm.napakaNaziv = "";

            if(!vm.vir.naziv){
                vm.napakaNaziv = "Zahtevan vsaj 1 znak!";
                napaka = true;
            }

            if (napaka) {
                vm.napakaNaObrazcu = "Prišlo je do napake!";
                return false;
            } else {
                vm.posodabljanjeVira();
                
            }
        };

        vm.posodabljanjeVira = function() {
            vm.vir.ikona = "https://www."+extractHostname(vm.vir.url).replace(/^(www\.)/,"")+"/favicon.ico",
            viri.posodobiVir(vm.vir).then(

                function success(odgovor) {

                    vm.uspesnoDodan = "Vir je uspešno posodobljen";
                    vm.uspesnoDodanBool = true;
                    $timeout(function() {
                        // db.osveziRSS();
                        // $location.path('/admin');
                        // $route.reload();
                        posodobiCallback(vm.vir._id, vm.vir.naziv);
                        $uibModalStack.dismissAll();
                    }, 1000);   
                },
                function(napaka) {
                    vm.napakaNaobrazcu = "Prišlo je do napake!";
                }
            );
        };

        vm.show = true;

        vm.closeAlert = function(index) {
            vm.show = false;
        };
    }

    function extractHostname(url) {
        var hostname;

        if (url.indexOf("//") > -1) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }
        
        hostname = hostname.split(':')[0];
        hostname = hostname.split('?')[0];

        return hostname;
    }

    modalnoOknoPosodabljanjeController.$inject = ['viri', 'avtentikacija', 'db', '$location', '$uibModal', '$route', '$timeout', '$rootScope', 'zbraniVir', '$uibModalStack', 'posodobiCallback'];

    angular
        .module('newsCatcher')
        .controller('modalnoOknoPosodabljanjeController', modalnoOknoPosodabljanjeController);
})();