(function() {
    function brskanjeVirovController($route, viri, avtentikacija, uporabniki) {
        var vm = this;
        
        vm.sporocilo = "Brskanje virov.";
        
        vm.jePrijavljen = avtentikacija.jePrijavljen();
        
        vm.naStran = 10;
        vm.trenutnaStran = 1;
        vm.steviloStrani = 1;
        vm.isciVir = '';

        var prvic = true;
        vm.steviloVirov = 1;

        vm.viriFilter = function(vir) {
            if (vm.steviloVirov > vm.naStran) 
                return true;

            return (vir.naziv.toLowerCase().indexOf(vm.isciVir.toLowerCase()) !== -1)
        }
        
        vm.vrniVire = function(stran, naStran) {
            vm.trenutnaStran = stran > 0 ? stran : 1;
            vm.trenutnaStran =  vm.trenutnaStran < vm.steviloStrani ? vm.trenutnaStran : vm.steviloStrani;

            viri.vrniVire(vm.trenutnaStran, naStran, vm.isciVir).then(
                function success(odgovor) {
                    vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
                    vm.viri = odgovor.data.viri;
                    vm.steviloStrani = odgovor.data.steviloStrani;
                    if (prvic) {
                        vm.steviloVirov = odgovor.data.steviloVirov;
                        prvic = false;
                    }
                }, function error(odgovor) {
                    vm.sporocilo = "Prišlo je do napake!";
                    console.log(odgovor.e);
                }
            );
        }

        vm.vrniUporabnika = function(idUporabnika) {
            uporabniki.vrniUporabnika(idUporabnika).then(
                function success(odgovor) {
                    vm.trenutniUporabnik = odgovor.data;
                }, function error(odgovor) {
                    vm.napakaNaObrazcu = "Prislo je do napake!";
                }
            );
        }
        
        vm.idUporabnika = avtentikacija.trenutniUporabnik().id;


        vm.vrniUporabnika(vm.idUporabnika);
    
        // viri.vrniVire(vm.trenutnaStran, vm.naStran, vm.isciVir).then(
        //         function success(odgovor) {
        //             vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
        //             vm.viri = odgovor.data.viri;
        //             vm.steviloStrani = odgovor.data.steviloStrani;
        //         }, function error(odgovor) {
        //             vm.sporocilo = "Prišlo je do napake!";
        //             console.log(odgovor.e);
        //         }
        //     );

        vm.vrniVire(vm.trenutnaStran, vm.naStran);
    
        // viri.vrniMojeVire(avtentikacija.trenutniUporabnik().id).then(
        //     function success(odgovor) {
        //         vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem virov.";
        //         $scope.mojiViri = odgovor.data.viri;
        //     }, function error(odgovor) {
        //         vm.sporocilo = "Prišlo je do napake!";
        //         console.log(odgovor.e);
        //     }
        // );
             
        vm.dodajanjeMojegaVira = function dodajanjeMojegaVira(idVira) {
            viri.dodajMojVir(avtentikacija.trenutniUporabnik().id, {idVira: idVira}).then(
                function success(odgovor) {
                    vm.vrniUporabnika(vm.idUporabnika);
                }, function error(odgovor) {
                    vm.sporocilo = "Prislo je do napake!";
                }
            );
        }
        
         vm.odjavaMojegaVira = function odjavaMojegaVira(idVira) {
            viri.odjaviMojVir(avtentikacija.trenutniUporabnik().id, idVira).then(
                function success(odgovor) {
                    vm.vrniUporabnika(vm.idUporabnika);
                }, function error(odgovor) {
                    vm.sporocilo = "Prislo je do napake!";
                }
            );
        }
        
        vm.preveriMojVir = function(idVira) {
            return vm.trenutniUporabnik.viri.includes(idVira);
        }
    }

    brskanjeVirovController.$inject = ['$route', 'viri', 'avtentikacija', 'uporabniki'];
    
    angular
        .module('newsCatcher')
        .controller('brskanjeVirovController', brskanjeVirovController);
})();