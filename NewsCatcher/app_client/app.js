(function() {

	var authUser = function(avtorizacija) {
		return avtorizacija.authenticate(['user']);
	};
	var authAdmin = function(avtorizacija) {
		return avtorizacija.authenticate(['admin']);
	};
	authUser.$inject = ['avtorizacija'];
	authAdmin.$inject = ['avtorizacija'];

	var run = function($rootScope, $location){
	    $rootScope.$on('$routeChangeError', function(event, current, previous, rejection){
	        if(rejection === 'Not Authenticated'){
	            console.log("Nimas pravic!!!");
	            $location.path('/');
	        }
	    })
	}

	run.$inject = ['$rootScope', '$location'];

	function nastavitev($routeProvider, $locationProvider) {
		$routeProvider
        // TO-DO: Redirect when logged in.
        // KDOR RABI PRIKAZ PRILJUBLJENIH VIROV NAJ TOLE ODKOMENTIRA IN ZAKOMENTIRA VIEW mojiViri !!!
            /*.when('/', {
                templateUrl: 'seznamVirov/seznamVirov.pogled.html',
                controller: 'seznamVirovController',
                controllerAs: 'vm'
             })*/
            .when('/', {
                templateUrl: 'mojiViri/root.pogled.html',
                controller: 'mojiViriController',
                controllerAs: 'vm'
             })
            /*.when('/viri/nov', {
                templateUrl: 'dodajanjeVirov/dodajanjeVirov.pogled.html',
                controller: 'dodajanjeVirovController',
                controllerAs: 'vm'
            })*/
            .when('/viri/brskaj', {
                templateUrl: 'brskanjeVirov/brskanjeVirov.pogled.html',
                controller: 'brskanjeVirovController',
                controllerAs: 'vm',
                resolve : {
	                'auth' : authUser,
	            }
            })
			.when('/viri/:idVira', {
				templateUrl: 'seznamNovic/seznamNovic.pogled.html',
				controller: 'seznamNovicController',
				controllerAs: 'vm'
			 })
			.when('/prijava', {
	            templateUrl: 'prijava/prijava.pogled.html',
	            controller: 'prijavaController',
	            controllerAs: 'vm'
	        })
	        .when('/registracija', {
	            templateUrl: 'registracija/registracija.pogled.html',
	            controller: 'registracijaController',
	            controllerAs: 'vm'
	        })
            .when('/uporabnik', {
                templateUrl: 'uporabniki/uporabniki.pogled.html',
                controller: 'uporabnikiController',
                controllerAs: 'vm',
                resolve : {
	                'auth' : authUser,
	            }
            })
            .when('/admin', {
                templateUrl: 'admin/admin.pogled.html',
                controller: 'adminController',
                controllerAs: 'vm',
                resolve : {
	                'auth' : authAdmin,
	            }
            })
	        .when('/db', {
	            templateUrl: 'db/db.pogled.html',
	            controller: 'dbController',
	            controllerAs: 'vm'
	        })
	    	.otherwise({redirectTo: '/'});
	    	$locationProvider.html5Mode(true);
	}		

	angular
		.module('newsCatcher', ['ngRoute', 'ui.bootstrap'])
		.config(['$routeProvider', '$locationProvider', nastavitev])
		.run(run);
})();
