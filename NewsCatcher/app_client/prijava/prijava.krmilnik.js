(function() {
    function prijavaController(avtentikacija, $location) {
        var vm = this;

        vm.uporabnik = {
          email: "",
          geslo: ""
        };

        vm.prijaviUporabnika = function() {
            var napaka = false;
            vm.napakaNaObrazcu = "";
            vm.napakaEmail = "";
            vm.napakaGeslo = "";

            if(!vm.uporabnik.email){
                vm.napakaEmail = "Napačen vnos oz. napačen email!";
                napaka = true;
            }

            if(!vm.uporabnik.geslo){
                vm.napakaGeslo = "Napačen vnos!";
                napaka = true;
            }

            if (napaka) {
                vm.napakaNaObrazcu = "Prišlo je do napake!";
                return false;
            } else {
                vm.izvediPrijavo();
            }
        };

        vm.izvediPrijavo = function() {
            avtentikacija.prijava(vm.uporabnik).then(
                function(success) {
                    $location.path('/');
                },
                function(napaka) {
                    vm.napakaNaObrazcu = "Prišlo je do napake pri vsaj enem od vnosnih polj!";
                    $location.path('/prijava');
                }
            );
        };
    }

    prijavaController.$inject = ['avtentikacija', '$location'];

    angular
        .module('newsCatcher')
        .controller('prijavaController', prijavaController)
        
})();