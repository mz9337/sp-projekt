(function() {
    function dodajanjeVirovController(viri, $location) {
        var vm = this;

        vm.sporocilo = "Dodajanje vira.";
        function dodajanjeVira() {
            viri.dodajVir(vm.vir, function(odgovor) {
                console.log("vm.vir: " + vm.vir);
                if (odgovor == 200) {
                    console.log("Uspešno dodajanje vira");
                    $location.path('/');
                } else {
                    console.log("Napaka");
                    vm.sporocilo = "Prišlo je do napake!";
                }
            });
        }
        vm.dodajanjeVira = dodajanjeVira;
    }

    dodajanjeVirovController.$inject = ['viri', '$location'];

    angular
        .module('newsCatcher')
        .controller('dodajanjeVirovController', dodajanjeVirovController);
})();