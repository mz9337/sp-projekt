(function() {
    function modalnoOknoController(viri, avtentikacija, db, $location, $uibModal, $route, $timeout, $rootScope, $uibModalStack, callback) {
        var vm = this;

        vm.vir = {
            url: "",
            naziv: "",
            opis: ""
        };  
        
        vm.posiljanjePodatkov = function() {
            var napaka = false;
            vm.napakaNaObrazcu = "";
            vm.napakaUrl = "";
            vm.napakaNaziv = "";

            if(!vm.vir.url){
                vm.napakaUrl = "Vnesite pravilen URL: http://example.exmpl/optional";
                napaka = true;
            }

            if(!vm.vir.naziv){
                vm.napakaNaziv = "Zahtevan vsaj 1 znak!";
                napaka = true;
            }

            if (napaka) {
                vm.napakaNaObrazcu = "Prišlo je do napake!";
                return false;
            } else {
                vm.dodajanjeVira();
                
            }
        };

        vm.dodajanjeVira = function() {
            vm.vir.ikona = "https://www."+extractHostname(vm.vir.url)+"/favicon.ico",
            viri.dodajVir(vm.vir).then(

                function success(odgovor) {
                    // console.log(avtentikacija.trenutniUporabnik().id);

                    vm.uspesnoDodan = "Vir je uspešno dodan";
                    vm.uspesnoDodanBool = true;
                    $timeout(function() {
                        vm.uspesnoDodanBool = false;

                        viri.dodajMojVir(avtentikacija.trenutniUporabnik().id, {idVira: odgovor.data._id}).then(
                             function success(odgovor) {
                             },
                             function error(odgovor) {
                             }
                        );
                        db.osveziRSS();

                        $location.path('/');
                        $uibModalStack.dismissAll();

                        callback();
                        // $route.reload();
                    }, 1000);   
                },
                function(napaka) {
                    vm.napakaNaobrazcu = "Prišlo je do napake!";
                }
            );
        };

        vm.show = true;

        vm.closeAlert = function(index) {
            vm.show = false;
        };
    }

    function extractHostname(url) {
        var hostname;

        if (url.indexOf("//") > -1) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }
        
        hostname = hostname.split(':')[0];
        hostname = hostname.split('?')[0];

        return hostname;
    }

    modalnoOknoController.$inject = ['viri', 'avtentikacija', 'db', '$location', '$uibModal', '$route', '$timeout', '$rootScope', '$uibModalStack', 'callback'];

    angular
        .module('newsCatcher')
        .controller('modalnoOknoController', modalnoOknoController);
})();