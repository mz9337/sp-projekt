require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cron = require('node-cron');
var rssController = require('./app_server/controllers/rss');
var uglifyJs = require('uglify-js');
var fs = require('fs');

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
  'db.krmilnik.js': fs.readFileSync('app_client/db/db.krmilnik.js', 'utf-8'),
  'avtentikacija.storitev.js': fs.readFileSync('app_client/skupno/storitve/avtentikacija.storitev.js', 'utf-8'),
  'db.storitev.js': fs.readFileSync('app_client/skupno/storitve/db.storitev.js', 'utf-8'),
  'uporabniki.storitev.js': fs.readFileSync('app_client/skupno/storitve/uporabniki.storitev.js', 'utf-8'),
  'viri.storitev.js': fs.readFileSync('app_client/skupno/storitve/viri.storitev.js', 'utf-8'),
  'navigacija.direktiva.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.direktiva.js', 'utf-8'),
  'navigacija.krmilnik.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.krmilnik.js', 'utf-8'),
  'noga.direktiva.js': fs.readFileSync('app_client/skupno/direktive/noga/noga.direktiva.js', 'utf-8'),
  'uporabniki.krmilnik.js': fs.readFileSync('app_client/uporabniki/uporabniki.krmilnik.js', 'utf-8'), 
  'modalnoOkno.krmilnik.js': fs.readFileSync('app_client/modalnoOkno/modalnoOkno.krmilnik.js', 'utf-8'),
  'modalnoOknoPosodabljanje.krmilnik.js': fs.readFileSync('app_client/modalnoOknoPosodabljanje/modalnoOknoPosodabljanje.krmilnik.js', 'utf-8'),
  'mojiViri.krmilnik.js': fs.readFileSync('app_client/mojiViri/mojiViri.krmilnik.js', 'utf-8'),
  'dodajanjeVirov.krmilnik.js': fs.readFileSync('app_client/dodajanjeVirov/dodajanjeVirov.krmilnik.js', 'utf-8'),
  // 'seznamVirov.krmilnik.js': fs.readFileSync('app_client/mojiViri/seznamVirov.krmilnik.js', 'utf-8'),
  'prijava.krmilnik.js': fs.readFileSync('app_client/prijava/prijava.krmilnik.js', 'utf-8'),
  'registracija.krmilnik.js': fs.readFileSync('app_client/registracija/registracija.krmilnik.js', 'utf-8'),
  'seznamNovic.krmilnik.js': fs.readFileSync('app_client/seznamNovic/seznamNovic.krmilnik.js', 'utf-8'),
  'brskanjeVirov.krmilnik.js': fs.readFileSync('app_client/brskanjeVirov/brskanjeVirov.krmilnik.js', 'utf-8'),
  'admin.krmilnik.js': fs.readFileSync('app_client/admin/admin.krmilnik.js', 'utf-8'),
  'prikaziInspirationalQuote.direktiva.js': fs.readFileSync('app_client/skupno/direktive/prikaziInspirationalQuote/prikaziInspirationalQuote.direktiva.js', 'utf-8'),
  'formatirajDatum.filter.js': fs.readFileSync('app_client/skupno/filtri/formatirajDatum.filter.js', 'utf-8'),
  'formatirajUro.filter.js': fs.readFileSync('app_client/skupno/filtri/formatirajUro.filter.js', 'utf-8'),
  'avtorizacija.tovarna.js': fs.readFileSync('app_client/skupno/tovarne/avtorizacija.tovarna.js', 'utf-8')
});

fs.writeFile('public/angular/newsCatcher.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "newsCatcher.min.js".');
});

const session =  require('express-session') ;

var passport = require('passport');

require('./app_api/models/db');
require('./app_api/konfiguracija/passport');

//var indexRouter = require('./app_server/routes/index');
//var viriRouter = require('./app_server/routes/viri');

var indexApi = require('./app_api/routes/index');

var app = express();

var appendLocalsToUseInViews = function(req, res, next) {
    res.locals.request = req;

    if(req.session != null && req.session.userId != null) res.locals.userId = req.session.userId;

    next(null, req, res);
};

cron.schedule('0 0 */4 * * *', () => {
	rssController.parseRss();
});

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

//odprava težav s HTTP odgovori
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'deny');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.use(session({ secret: 'uporabnikId', resave: false, saveUninitialized: true }));

app.use(appendLocalsToUseInViews);

//app.use('/', indexRouter); 
app.use('/api', indexApi); 
// app.use('/viri', viriRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
