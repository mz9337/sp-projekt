const mongoose = require('mongoose');

let uporabnikiShema = new mongoose.Schema({
    ime: {type: String, required: true},                
    priimek: {type: String, required: true},
    uporabniskoIme: {type: String, required: true},
    email: {type: String, required: true},
    geslo: {type: String, required: true},
});

mongoose.model('Uporabnik', uporabnikiShema);