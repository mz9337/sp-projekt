var express = require('express');
var router = express.Router();
var viriController = require('../controllers/viri');

// TODO implementacija route-ov za vire

router.get('/brskaj', function (req, res, next) {
	if (req.session.userId) {
		viriController.brskaj(req, res);
	}else {
		return res.redirect('/');
	}
});

router.post('/naroci', function (req, res, next) {
	if (req.session.userId) {
		viriController.naroci(req, res);
	}else {
		return res.redirect('/');
	}
});

router.post('/odjavi', function (req, res, next) {
	if (req.session.userId) {
		viriController.odjavi(req, res);
	}else {
		return res.redirect('/');
	}
});

router.get('/nov', function (req, res, next) {
	if (req.session.userId) {
		viriController.nov(req, res);
	}else {
		return res.redirect('/');
	}
});

router.post('/nov', function (req, res, next) {
	if (req.session.userId) {
		viriController.novPost(req, res);
	}else {
		return res.redirect('/');
	}
});

router.get('/:id', viriController.prikaziSeznamNovic);
router.post('/oceni', viriController.oceniVir);

module.exports = router;
