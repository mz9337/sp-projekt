var express = require('express');
var router = express.Router();
var viriController = require('../controllers/viri');
var prijavaController = require('../controllers/prijava');
var uporabnikiController = require('../controllers/uporabniki');
var bazaController = require('../controllers/baza');
var mainController = require('../controllers/main');


router.get('/', mainController.angularApp);


// router.get('/', function (req, res, next) {
// 	if (req.session.userId) {
// 		viriController.domov(req, res);
// 	}else {
// 		viriController.seznam(req, res);
// 	}
// });

// router.get('/uporabnik', function (req, res, next) {
// 	if (req.session.userId) {
// 		uporabnikiController.uporabnikiProfil(req, res);
// 	}else {
// 		return res.redirect('/');
// 	}
// });

// router.post('/uporabnik', function (req, res, next) {
// 	if (req.session.userId) {
// 		uporabnikiController.uporabnikiPosodobi(req, res);
// 	}else {
// 		return res.redirect('/');
// 	}
// });

// router.get('/prijava', prijavaController.prijava);
// router.post('/prijava', prijavaController.prijavaPost);
// router.get('/registracija', prijavaController.registracija);
// router.post('/registracija', prijavaController.registracijaPost);
// router.get('/db', bazaController.index);
// router.get('/odjava', prijavaController.odjava);

module.exports = router;
