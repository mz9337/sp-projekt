var rssController = require('./rss');
var request = require('request');
var extractDomain = require('extract-domain');
var apiParametri = {
	streznik: 'http://localhost:' + (process.env.PORT || 3000)
};

if (process.env.NODE_ENV === 'production') {
	apiParametri.streznik = 'https://bralnik-rss-virov.herokuapp.com';
}

var prikaziSeznamNovic = function(req, res, vsebina) {
    res.render('seznam-novic', {
        vir: vsebina
    });
};

module.exports.seznam = function(req, res) {

    var pot = '/api/viri';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'GET',
		json: {},
	};
	request(
		parametriZahteve,
		function(napaka, odgovor,  vsebina) {
			res.render('viri-seznam', {
			    viri: vsebina
			});  
		}
	);
};

module.exports.brskaj = function(req, res) {

    var idUporabnika = req.session.userId

    var pot = '/api/uporabniki/'+idUporabnika;
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'GET',
		json: {},
	};
	request(
		parametriZahteve,
		function(napaka, odgovor,  uporabnik) {
			// res.render('viri-domov', {
			//     viri: vsebina
			// }); 

			var mojiViri = uporabnik.viri;

			pot = '/api/viri';
			
			parametriZahteve = {
				url: apiParametri.streznik + pot,
				method: 'GET',
				json: {},
			};
			request(
				parametriZahteve,
				function(napaka, odgovor,  viri) {
					res.render('viri-brskaj', {
					    viri: viri,
					    mojiViri: mojiViri
					});  
				}
			);
		}
	);

    // res.render('viri-brskaj', viri_data);
};

module.exports.domov = function(req, res) {

    var idUporabnika = req.session.userId

    var pot = '/api/uporabniki/'+idUporabnika+'/viri';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'GET',
		json: {},
	};
	request(
		parametriZahteve,
		function(napaka, odgovor,  vsebina) {
			res.render('viri-domov', {
			    viri: vsebina
			});  
		}
	);
};

module.exports.prikaziSeznamNovic = function(req, res) {
    /* LP 2 klici
    viri_data = novice;
    res.render('seznam-novic', viri_data);
    */
    
    var pot = '/api/viri/' + req.params.id;
    
    // console.log(apiParametri.streznik + pot);
    
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
        json: {},
    };
    // console.log(parametriZahteve);
    request(
        parametriZahteve,
        function(napaka, odgovor, novice) {
            prikaziSeznamNovic(req, res, novice);
        }
    );
};

module.exports.naroci = function(req, res) {

    var idUporabnika = req.session.userId;

    var idVira = req.body.idVira;

	var pot = '/api/uporabniki/' + idUporabnika + '/viri';

	var nazajUrl = req.header('Referer') || '/';

	var posredovaniPodatki = {
	    idVira: req.body.idVira
	};
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'POST',
		json: posredovaniPodatki,
	};

	request(
		parametriZahteve,
		function(napaka, odgovor, vsebina) {
			return res.redirect(nazajUrl);
		}
	);
}

module.exports.odjavi = function(req, res) {
	
    var idUporabnika = req.session.userId;

    var idVira = req.body.idVira;

	var pot = '/api/uporabniki/' + idUporabnika + '/viri/' + idVira;

	var nazajUrl = req.header('Referer') || '/';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'DELETE',
		json: {},
	};

	request(
		parametriZahteve,
		function(napaka, odgovor, vsebina) {
			return res.redirect(nazajUrl);
		}
	);
}

module.exports.novPost = function(req, res) {

    var pot = '/api/viri/';

    var nazajUrl = req.header('Referer') || '/';

	var posredovaniPodatkiVir = {
		naziv: req.body.naziv,
		url: req.body.url,
		opis: req.body.opis,
        ikona: "http://www."+extractDomain(req.body.url)+"/favicon.ico",
		novice: [],
		ocena: []
	};

	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'POST',
		json: posredovaniPodatkiVir,
	};

    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
        	rssController.parseRss();
            return res.redirect("/viri/brskaj");
        }
    );

};

module.exports.nov = function(req, res) {
    res.render('viri-dodaj-nov', {});
};

module.exports.oceniVir = function(req, res) {
    var idUporabnika = req.session.userId;
    
    var idVira = req.body.idVira;
    
    var pot = '/api/viri/' + idVira + '/ocene/' + idUporabnika;
    var pot2 = '/api/viri/' + idVira + '/ocene';
    
    var nazajUrl = req.header('Referer') || '/';
    
    var posredovaniPodatki = {
        idVira: req.body.idVira,
        idUporabnika: req.session.userId,
        idOsebe: req.session.userId,
        ocena: req.body.ocena
    }
    
        var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
    };
    
    
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
            if (odgovor.statusCode == '404') {
                parametriZahteve = {
                    url: apiParametri.streznik + pot2,
                    method: 'POST',
                    json: posredovaniPodatki
                };
                
            } else {
                parametriZahteve = {
                    url: apiParametri.streznik + pot,
                    method: 'PUT',
                    json: posredovaniPodatki
                };
            }
                request(
                    parametriZahteve,
                    function(napaka, odgovor, vsebina) {
                        return res.redirect(nazajUrl);
                    }
                );     
        }
    )
}