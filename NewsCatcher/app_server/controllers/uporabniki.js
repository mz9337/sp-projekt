var request = require('request');
var apiParametri = {
    streznik: 'http://localhost:' + (process.env.PORT || 3000)
};

if (process.env.NODE_ENV === 'production') {
    apiParametri.streznik = 'https://bralnik-rss-virov.herokuapp.com';
}

module.exports.uporabnikiProfil = function(req, res) {
    
    var idUporabnika = req.session.userId

    var pot = '/api/uporabniki/' + idUporabnika;

    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
        json: {},
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, data) {
            res.render('uporabnik-profil', {
                ime: data.ime,
                priimek: data.priimek,
                uporabniskoIme: data.uporabniskoIme,
                email: data.email,
                password: data.password
            });
        }
    );
};

module.exports.uporabnikiPosodobi= function(req, res) {

    var idUporabnika = req.session.userId

    var pot = '/api/uporabniki/' + idUporabnika;

    var nazajUrl = req.header('Referer') || '/';

    var posredovaniPodatki = {
        idUporabnika: idUporabnika,
        ime: req.body.ime,
        priimek: req.body.priimek,
        uporabniskoIme: req.body.uporabniskoIme,
        email: req.body.email,        
    };

    if (req.body.geslo && typeof req.body.geslo !== "undefined") {
        posredovaniPodatki.geslo = req.body.geslo;
    }

    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'PUT',
        json: posredovaniPodatki,
    };

    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
            return res.redirect(nazajUrl);
        }
    );

};