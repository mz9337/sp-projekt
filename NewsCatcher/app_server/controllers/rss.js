var request = require('request');
var xmlParser = require("xml-parse");
var parseString = require('xml2js').parseString;

var apiParametri = {
	streznik: 'http://localhost:' + (process.env.PORT || 3000)
};

if (process.env.NODE_ENV === 'production') {
	apiParametri.streznik = 'https://bralnik-rss-virov.herokuapp.com';
}

module.exports.parseRss = function() {
	var pot = '/api/viri?naStran=99999';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'GET',
		json: {},
	};
	request(
		parametriZahteve,
		function(napaka, odgovor, viri) {
			if (typeof viri === "undefined" || typeof viri.viri === "undefined") return;

			viri.viri.forEach(function(vir) {
				var url = vir.url;

				var novice = vir.novice;

				// console.log(novice);

				// vir.novice.forEach(function(novica) {
				// 	request(
				// 		{
				// 			url: apiParametri.streznik + '/api/viri/'+vir._id+'/novice/'+novica._id,
				// 			method: 'DELETE',
				// 		},

				// 		function(napaka, odgovor, xml) {
				// 		}
				// 	)
				// });

				request(
					{
						url: url,
						method: 'GET',
					},

					function(napaka, odgovor, xml) {
						parseString(xml, function (err, result) {
							if (typeof result != "undefined" && typeof result.rss != "undefined") {
								result.rss.channel.forEach(function(channel) {
									var title = channel.title;
									var url = channel.url;

									// var limit = 10;

							    	channel.item.forEach(function(item) {
							    		// if (limit == 0) return;

							    		var title = (typeof item.title !== "undefined" && item.title.length) > 0 ? item.title[0] : '';
							    		var description = (typeof item.description !== "undefined" && item.description.length) > 0 ? item.description[0] : '';
							    		var urlNovice = (typeof item.link !== "undefined" && item.link.length) > 0 ? item.link[0] : '';
							    		var date = (typeof item.pubDate !== "undefined" && item.pubDate.length) > 0 ? item.pubDate[0] : '';

							    		var description = description.replace(/(<([^>]+)>)/ig,"");

							    		var image = '';

							    		if (typeof item['media:thumbnail'] != "undefined") {
							    			image = item['media:thumbnail'][0]['$']['url'];
							    		}
							    		if (typeof item['enclosure'] != "undefined") {
							    			image = item['enclosure'][0]['$']['url'];
							    		}

							    		var novica = {
							    			naziv: title,
							    			opis: description,
							    			url: urlNovice,
							    			datumObjave: date,
							    			slika: image,
							    		}


							    		if (typeof novice.find(x => x.naziv == novica.naziv) === "undefined") {
								    		request(
												{
													url: apiParametri.streznik + '/api/viri/'+vir._id+'/novice',
													method: 'POST',
													json: novica,
												},

												function(napaka, odgovor, xml) {
												}
											)
							    		}
							    		// limit--;
							    	});
								});
							}
						});
					}
				);
			});
		}
	);
};