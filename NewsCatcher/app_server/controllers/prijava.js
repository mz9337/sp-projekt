var request = require('request');
//const dataJSON_uporabniki = require('../models/model_uporabniki');
//const mongoose = require('mongoose')
//const uporabnikiModel = mongoose.model('Uporabnik');
const crypto = require('crypto');

var apiParametri = {
	streznik: 'http://localhost:' + (process.env.PORT || 3000)
};

if (process.env.NODE_ENV === 'production') {
	apiParametri.streznik = 'https://bralnik-rss-virov.herokuapp.com';
}

module.exports.prijava = function(req, res) {

    res.render('prijava');
};

module.exports.prijavaPost = function(req, res) {
	var uporabnikTrue = 0;
	var sess = req.session;

	var email = req.body.email;
	var geslo = req.body.geslo;
	const hash = crypto.createHmac('sha256', 'test').update(geslo).digest('hex');

	var pot = '/api/uporabniki';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'GET',
		json: {},
	};
	request(
		parametriZahteve,
		function(napaka, odgovor, uporabniki) {
			if (napaka != null) {
				return res.json({ napaka: napaka });
			}

			uporabniki.forEach(function(uporabnik) {
				if (uporabnik.email == email && uporabnik.geslo == hash) {
					uporabnikTrue = 1;					
					sess.userId = uporabnik._id;

					return res.redirect('/');				
				}
			});

			if (uporabnikTrue == 0) {
				return res.redirect('/prijava'); 
			}
		}
	);
}

module.exports.registracija = function(req, res) {

    res.render('registracija');
};

function preveriString(vnos) {
    var ri = /[A-Za-z]+/;
    return ri.test(String(vnos).toLowerCase());
}

function preveriEmail(email) {
    var ri = /[A-Za-z]+@[a-z]+\.com/;
    return ri.test(String(email).toLowerCase());
}

module.exports.registracijaPost = function(req, res) {
	//var sess = req.session;

	var ime = req.body.ime;
	var priimek = req.body.priimek;
	var email = req.body.email;
	var upIme = req.body.uporabniskoIme;
	var geslo = req.body.geslo;

	const hash = crypto.createHmac('sha256', 'test')
						.update(geslo)
						.digest('hex');
    

	var pot = '/api/uporabniki';
			
	var parametriZahteve = {
		url: apiParametri.streznik + pot,
		method: 'POST',
		json: { 
			ime: ime,
			priimek: priimek,
			email: email,
			uporabniskoIme: upIme,
			geslo: hash
		},
	};

	if (!preveriString(ime)) {
		return res.redirect('/registracija');
	}

	if (!preveriString(priimek)) {
		return res.redirect('/registracija');
	}

	if (!preveriEmail(email)) {
		return res.redirect('/registracija');
	}

	request(
		parametriZahteve,
		function(napaka, odgovor, uporabniki) {
			if (napaka != null) {
				return res.redirect('/registracija');
			} else {
				return res.redirect('/prijava'); 
			}
		}
	);
};

module.exports.odjava = function(req, res) {
	var sess = req.session;

	sess.destroy(function(err) {
		if (err) {
			return res.json({ err: err });
		}
	});

	return res.redirect('/');
};